﻿using UnityEngine;

namespace nkjzm.SafeAreaCanvas
{
    [ExecuteInEditMode()]
    public class SetCanvasBounds : MonoBehaviour
    {
        public RectTransform panel;
        Rect lastSafeArea = new Rect(0, 0, 0, 0);

        void ApplySafeArea(Rect area)
        {
            panel.anchoredPosition = Vector2.zero;
            panel.sizeDelta = Vector2.zero;

            var anchorMin = area.position;
            var anchorMax = area.position + area.size;
            anchorMin.x /= Screen.width;
            anchorMin.y /= Screen.height;
            anchorMax.x /= Screen.width;
            anchorMax.y /= Screen.height;
            panel.anchorMin = anchorMin;
            panel.anchorMax = anchorMax;

            lastSafeArea = area;
        }

        private void Awake()
        {
            if (panel == null) { return; }

            Rect safeArea = GetSafeArea();

            if (safeArea != lastSafeArea)
            {
                ApplySafeArea(safeArea);
            }
        }

        private void OnEnable()
        {
            if (panel == null) { return; }

            Rect safeArea = GetSafeArea();

            if (safeArea != lastSafeArea)
            {
                ApplySafeArea(safeArea);
            }
        }

        void Update()
        {
            if (panel == null) { return; }

            Rect safeArea = GetSafeArea();

#if UNITY_EDITOR
            if (Screen.width == 1125 && Screen.height == 2436)
            {
                safeArea.y = 102;
                safeArea.height = 2202;
            }
            if (Screen.width == 2436 && Screen.height == 1125)
            {
                safeArea.x = 132;
                safeArea.y = 63;
                safeArea.height = 1062;
                safeArea.width = 2172;
            }
#endif
            if (safeArea != lastSafeArea)
            {
                ApplySafeArea(safeArea);
            }
        }

        Rect GetSafeArea()
        {
            Rect safeArea = Screen.safeArea;
            float safeAreaWidth = Screen.width - safeArea.width;

            // 偏りがひどいセーフエリアに対して中央寄せを行う対応
            if (safeAreaWidth == Screen.safeArea.x)             // 左が異常に広い
            {
                safeArea.width -= Screen.safeArea.x;
            }
            else if (Screen.width != safeArea.width && Screen.safeArea.x == 0)　// 右が異常に広い
            {
                safeArea.x = Screen.width - safeArea.width;
                safeArea.width -= safeAreaWidth;
            }

            return safeArea;
        }
    }
}