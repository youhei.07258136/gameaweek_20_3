﻿using UniRx.Async;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Profiling;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// StreamingAssetsからの読み込み関連
/// </summary>
static public class StreamingAssetsLoader
{
	/// <summary>
	/// The clollect allocated memory.
	/// </summary>
    private const long ClollectAllocatedMemory = 128 * 1024 * 1024;

	/// <summary>
	/// The texture cache.
	/// </summary>
    static private Dictionary<string, Texture2D> textureCache = new Dictionary<string, Texture2D>();

    /// <summary>
    /// SteamingAssetsからバイナリ読み込み
    /// </summary>
    /// <param name="path">パス</param>
    /// <returns></returns>
    static public async UniTask<byte[]> LoadStreamingAssetsData(string path)
    {
		byte[] ret = null;
        string filePath = Application.streamingAssetsPath + "/" + path;

		// MacのEditorでStreamingAssetsアクセスにWebRequest出来なかったので
		// エディタとPC動作はアクセス権の問題も薄いことから直でファイルアクセスするように変更
#if UNITY_EDITOR || UNITY_STANDALONE
		await UniTask.Run(() => 
		{
			using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
			{
				ret = new byte[fs.Length];
				fs.Read(ret, 0, (int)fs.Length);
			}
		});
#else
        var op = await UnityWebRequest.Get(filePath).SendWebRequest();
		ret = op.downloadHandler.data;
		op.Dispose();
#endif

		return ret;
    }

    /// <summary>
    /// SteamingAssetsからテキスト読み込み
    /// </summary>
    /// <param name="path">パス</param>
    /// <returns></returns>
    static public async UniTask<string> LoadStreamingAssetsText(string path)
    {
		string ret = "";
        string filePath = Application.streamingAssetsPath + "/" + path;

		// MacのEditorでStreamingAssetsアクセスにWebRequest出来なかったので
		// エディタとPC動作はアクセス権の問題も薄いことから直でファイルアクセスするように変更
#if UNITY_EDITOR || UNITY_STANDALONE
		await UniTask.Run(() =>
		{
			using(StreamReader sr = new StreamReader(filePath))
			{
				ret = sr.ReadToEnd();
			}
		});
#else
        var op = await UnityWebRequest.Get(filePath).SendWebRequest();
		ret = op.downloadHandler.text;
		op.Dispose();
#endif

		return ret;
    }

    /// <summary>
    /// SteamingAssetsからテクスチャ読み込み
    /// </summary>
    /// <param name="path">パス</param>
    /// <param name="width">横幅</param>
    /// <param name="height">立幅</param>
    /// <param name="useCache">キャッシュ使うか</param>
    /// <returns></returns>
    static public async UniTask<Texture2D> LoadStreamingAssetsTexture(string path, int width, int height, bool useCache = true)
    {
        Texture2D ret;
        if (useCache && textureCache.TryGetValue(path, out ret))
        {
            return ret;
        }

        UniTask<byte[]> data = LoadStreamingAssetsData(path);

#if UNITY_ANDROID && !UNITY_EDITOR
        // Android = ASTC6x6
        ret = new Texture2D(width, height, UnityEngine.Experimental.Rendering.GraphicsFormat.RGBA_ASTC6X6_SRGB, 0);
#elif UNITY_IOS && !UNITY_EDITOR
        // warning : この判定で動くか未検証（動いてほしい）
        // iPhone5SならETC2でいい...と思う多分
        // http://tsubakit1.hateblo.jp/entry/2017/11/22/001941
        if((int)iOS.Device.generation >= (int)iOS.DeviceGeneration.iPhone6)
            ret = new Texture2D(width, height, UnityEngine.Experimental.Rendering.GraphicsFormat.RGBA_ASTC6X6_SRGB, 0)
        if ((int)iOS.Device.generation >= (int)iOS.DeviceGeneration.iPhone5s)
            ret = new Texture2D(width, height, UnityEngine.Experimental.Rendering.GraphicsFormat.RGBA_ETC2_SRGB, 0)
        else
          　Texture2D ret = new Texture2D(width, height, UnityEngine.Experimental.Rendering.GraphicsFormat.RGBA_PVRTC_4Bpp_SRGB, 0)
#elif UNITY_EDITOR || UNITY_STANDALONE
        ret = new Texture2D(width, height, UnityEngine.Experimental.Rendering.GraphicsFormat.RGBA_BC7_SRGB, 0);
#else
        ret = new Texture2D(width, height);
#endif

        await data;
        
        ret.LoadImage(data.Result);
        ret.wrapModeV = TextureWrapMode.Clamp;
        ret.wrapModeW = TextureWrapMode.Clamp;
        ret.Apply();

        if (useCache)
            textureCache[path] = ret;

        return ret;
    }

    /// <summary>
    /// SteamingAssetsからテクスチャ読み込み
    /// PNGファイル確定版(テクスチャの自動サイズ計算含む)
    /// </summary>
    /// <param name="path">パス</param>
    /// <param name="useCache">キャッシュ使うか</param>
    /// <returns></returns>
    static public async UniTask<Texture2D> LoadStreamingAssetsTextureFromPng(string path, bool useCache = true)
    {
        Texture2D ret;
        if (useCache && textureCache.TryGetValue(path, out ret))
        {
            return ret;
        }

        byte[] data = await LoadStreamingAssetsData(path);

        // バイナリから縦横の幅を取得
        // http://www.setsuki.com/hsp/ext/png.htm
        int pos = 16;
        int width = 0;
        for (int i = 0; i < 4; i++)
        {
            width = width * 256 + data[pos++];
        }

        int height = 0;
        for (int i = 0; i < 4; i++)
        {
            height = height * 256 + data[pos++];
        }

#if UNITY_ANDROID && !UNITY_EDITOR
        // Android = ASTC6x6
        ret = new Texture2D(width, height, UnityEngine.Experimental.Rendering.GraphicsFormat.RGBA_ASTC6X6_SRGB, 0);
#elif UNITY_IOS && !UNITY_EDITOR
        // warning : この判定で動くか未検証（動いてほしい）
        // iPhone5SならETC2でいい...と思う多分
        // http://tsubakit1.hateblo.jp/entry/2017/11/22/001941
        if((int)iOS.Device.generation >= (int)iOS.DeviceGeneration.iPhone6)
            ret = new Texture2D(width, height, UnityEngine.Experimental.Rendering.GraphicsFormat.RGBA_ASTC6X6_SRGB, 0)
        if ((int)iOS.Device.generation >= (int)iOS.DeviceGeneration.iPhone5s)
            ret = new Texture2D(width, height, UnityEngine.Experimental.Rendering.GraphicsFormat.RGBA_ETC2_SRGB, 0)
        else
          　Texture2D ret = new Texture2D(width, height, UnityEngine.Experimental.Rendering.GraphicsFormat.RGBA_PVRTC_4Bpp_SRGB, 0)
#elif UNITY_EDITOR || UNITY_STANDALONE
        ret = new Texture2D(width, height, UnityEngine.Experimental.Rendering.GraphicsFormat.RGBA_BC7_SRGB, 0);
#else
        ret = new Texture2D(width, height);
#endif

        ret.LoadImage(data);
        ret.wrapModeV = TextureWrapMode.Clamp;
        ret.wrapModeW = TextureWrapMode.Clamp;
        ret.Apply();

        if(useCache)
            textureCache[path] = ret;

        return ret;
    }

    /// <summary>
    /// キャッシュがUnityの許容メモリ使用量までたまってたら破棄する
    /// </summary>
    static public void ChacheCollect()
    {
        if(Profiler.GetTotalAllocatedMemoryLong() >= ClollectAllocatedMemory)
        {
            foreach(Texture2D tex in textureCache.Values)
            {
                Object.Destroy(tex);
            }

            textureCache.Clear();
        }
    }
}