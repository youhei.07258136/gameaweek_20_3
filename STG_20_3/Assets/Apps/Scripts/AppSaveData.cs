﻿using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using UnityEngine;

/// <summary>
/// User data.
/// </summary>
public static class AppSaveData
{
    private static readonly string CheckKey = "CheckHas"; //!< セーブデータの有無チェック用のキー
    private const int RankingNum = 9;

    [Serializable]
    public class RankingData
    {
        public string dateText;
        public long score;
    }

    [Serializable]
    public class ProfileData
    {
        public string name;
    }

    public static ProfileData Profile { get; private set; } = new ProfileData();
    public static List<RankingData> Rankings { get; private set; } = new List<RankingData>(); 

    /// <summary>
    /// Hases the user data.
    /// </summary>
    /// <returns><c>true</c>, if user data was hased, <c>false</c> otherwise.</returns>
    public static bool HasUserData()
    {
        return PlayerPrefs.GetInt(CheckKey, 0) == 1;
	}

    /// <summary>
	/// Initialize this instance.
	/// </summary>
	public static void Initialize()
    {
        Profile.name = "None";

        for(int i = 0; i < RankingNum; ++i)
        {
            RankingData r = new RankingData();
            r.score = 1000 * (RankingNum - i);
            r.dateText = "0000/00/00";
            Rankings.Add(r);
        }
    }

	/// <summary>
	/// Save this instance.
	/// </summary>
    public static void Save()
    {
        PlayerPrefs.SetInt(CheckKey, 1);
        PlayerPrefs.SetString(typeof(ProfileData).Name, Serialize(Profile));
        PlayerPrefs.SetString(typeof(RankingData).Name, Serialize(Rankings));
    }

	/// <summary>
	/// Load this instance.
	/// </summary>
    public static void Load()
    {
        if (!HasUserData())
        {
            Initialize();
            return;
        }

        Profile  = Deserialize<ProfileData>(PlayerPrefs.GetString(typeof(ProfileData).Name));
        Rankings = Deserialize<List<RankingData>>(PlayerPrefs.GetString(typeof(RankingData).Name));
    }

    /// <summary>
    /// スコアデータを記録
    /// </summary>
    /// <param name="score"></param>
    public static void AddScore(long score)
    {
        RankingData d = new RankingData();
        d.score = score;
        d.dateText = DateTime.Now.ToString("yyyy/MM/dd");

        Rankings.Add(d);
        Rankings.Sort((a, b) => b.score.CompareTo(a.score));

        Rankings.RemoveAt(RankingNum);
    }

	/// <summary>
	/// Serialize the specified obj.
	/// </summary>
	/// <returns>The serialize.</returns>
	/// <param name="obj">Object.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
    private static string Serialize<T>(T obj)
    {
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        MemoryStream memoryStream = new MemoryStream();
        binaryFormatter.Serialize(memoryStream, obj);
        return Convert.ToBase64String(memoryStream.GetBuffer());
    }

	/// <summary>
	/// Deserialize the specified str.
	/// </summary>
	/// <returns>The deserialize.</returns>
	/// <param name="str">String.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
    private static T Deserialize<T>(string str)
    {
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(str));
        return (T)binaryFormatter.Deserialize(memoryStream);
    }
}






