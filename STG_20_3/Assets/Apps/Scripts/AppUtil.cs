﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx.Async;
using UniRx.Toolkit;

public static class AppUtil
{
    public static Vector2 RotatePos(float rotateDeg, float distance)
    {
        Vector2 ret = new Vector2();
        ret.y = Mathf.Cos(rotateDeg * Mathf.Deg2Rad) * distance;
        ret.x = Mathf.Sin(rotateDeg * Mathf.Deg2Rad) * distance;

        return ret;
    }

    public static float PostToDeg(Vector2 pos)
    {
        return Mathf.Atan2(pos.x, pos.y) * Mathf.Rad2Deg;
    }

    public static async UniTask ReleasePoolAsync<T>(ObjectPool<T> pool) where T : UnityEngine.Component
    {
        float addRatio = 0.1f;

        await UniTask.WaitWhile(() =>
        {
            while (pool.Count > 0)
            {
                if (pool.Count < 10)
                {
                    pool.Clear();
                }
                else
                {
                    float ratio = pool.Count * addRatio;
                    pool.Shrink(ratio, 0);
                    addRatio += 0.1f;
                }

                return false;
            }

            pool.Dispose();

            return true;
        });
    }
}
