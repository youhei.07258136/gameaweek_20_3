﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;

/// <summary>
/// 決まった音を再生するボタン
/// </summary>
public class SeButton : Button
{
    /// <summary>
    /// 再生するSE種別
    /// </summary>
    public enum SeType
    {
        None,           //!< 無し
        Click,          //!< クリック音
        Cancel,         //!< キャンセル音
        DirectName      //!< 名前の直指定
    }

    private const string ClickSeCueName = "cmn_se_00";       //!< クリック音のキューネーム
    private const string CalcelSeCueName = "cmn_se_01";      //!< キャンセル音のキューネーム

    [Header("Se Button Setting")]
    [SerializeField] private SeType seType = SeType.Click;  //!< SE種別
    [SerializeField] private string cueName = default;      //!< Directの時だけ使うキュー名

    /// <summary>
    /// Awake
    /// </summary>
    protected override void Awake()
    {
        base.Awake();

        // SE再生のリスナー登録
        onClick.AddListener(() =>
        {
            switch(seType)
            {
                case SeType.Click:      SoundManager.Instance.PlaySeOneShot(ClickSeCueName);  break;
                case SeType.Cancel:     SoundManager.Instance.PlaySeOneShot(CalcelSeCueName); break;
                case SeType.DirectName: SoundManager.Instance.PlaySeOneShot(cueName);         break;
            }
        });
    }
}

