﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using UniRx;

/// <summary>
/// 子要素の色を乗算する
/// </summary>
[RequireComponent(typeof(Graphic))]
[DisallowMultipleComponent]
public class ColorGroup : MonoBehaviour
{
    /// <summary>
    /// 対象と元の色情報
    /// </summary>
    private class GraphicInfo
    {
        public Graphic graphic;     //!< 対象のグラフィック
        public Color defaultColor;  //!< 基本色
    }

    [SerializeField] private bool useButtonTarget = false;  //!< ボタンのtargtGraphicを使用するか？

    private List<GraphicInfo> tgtGraphics = new List<GraphicInfo>();    //!< 色を変える対象

    void Start()
    {
        // Graphicの取得
        Graphic srcGraphic = default;
        if (useButtonTarget)
            srcGraphic = GetComponent<Button>().targetGraphic;
        else
            srcGraphic = GetComponent<Graphic>();

        // 子要素を記録しておく
        foreach (Graphic tgt in GetComponentsInChildren<Graphic>(true).Where(c => gameObject != c.gameObject))
        {
            tgtGraphics.Add(new GraphicInfo() { graphic = tgt, defaultColor = tgt.color });
        }

        // CanvasRendererの色を監視して変動あれば子に適応
        srcGraphic
            .ObserveEveryValueChanged(col => srcGraphic.canvasRenderer.GetColor())
            .Subscribe(col =>
            {
                foreach(GraphicInfo tgt in tgtGraphics)
                {
                    tgt.graphic.color = tgt.defaultColor * col;
                }
            });
    }
}
