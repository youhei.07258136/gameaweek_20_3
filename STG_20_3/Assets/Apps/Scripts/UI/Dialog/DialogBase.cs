﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UniRx.Async;

/// <summary>
/// ダイアログの基底クラス
/// </summary>
public class DialogBase : MonoBehaviour
{
    [Header("Base Settings")]
    [SerializeField] protected Canvas canvasBg = default;                               //!< 背景用のキャンバス
    [SerializeField] protected Canvas canvasDialog = default;                           //!< ダイアログのキャンバス
    [SerializeField] protected Color mainColor = new Color(0, 1.0f, 186.0f / 255.0f);   //!< 枠などのメインカラー
    [SerializeField] protected RectTransform rtParts = default;                         //!< ダイアログの部品の親
    [SerializeField] protected Text txtTtile = default;                                 //!< タイトル
    [SerializeField] protected Image imgTitleUnderLine = default;                       //!< タイトルの下のライン
    [SerializeField] protected Image imgFrameLine = default;                            //!< 枠のライン
    [SerializeField] protected ScrollRect scrollRect = default;                         //!< スクロール
    [SerializeField] protected RectTransform rtScrollContent = default;                 //!< スクロールコンテンツの親
    [SerializeField] protected RectTransform rtNormalContent = default;                 //!< 通常コンテンツの親

    /// <summary>
    /// タイトル
    /// </summary>
    public string Title { get => txtTtile.text; set => txtTtile.text = value; }

    /// <summary>
    /// ダイアログを開く
    /// </summary>
    public void OpenDialog()
    {
        OpenASync();
    }

    /// <summary>
    /// ダイアログを閉じる
    /// </summary>
    public void CloseDialog()
    {
        rtParts.DOKill(true);
        rtParts.DOScale(Vector3.zero, 0.125f).SetEase(Ease.InCirc).OnComplete(()=> { OnCloseDialog(); Destroy(this.gameObject); });
    }

    /// <summary>
    /// 開く際の内部処理
    /// </summary>
    private async void OpenASync()
    {
        rtParts.localScale = Vector3.zero;

        await OnLoad();

        rtParts.DOKill(true);
        rtParts.DOScale(Vector3.one, 0.125f).SetEase(Ease.OutCirc);

        OnOpenDialog();
    }

    /// <summary>
    /// 開くときに何か読み込みたいときはここに記述
    /// </summary>
    /// <returns></returns>
    protected virtual async UniTask OnLoad()
    {
        await UniTask.Run(() => { });
    }

    /// <summary>
    /// 開いた時のイベント
    /// </summary>
    protected virtual void OnOpenDialog()
    {

    }

    /// <summary>
    /// 閉じたときのイベント
    /// </summary>
    protected virtual void OnCloseDialog()
    {

    }

    /// <summary>
    /// メインカラーの適応
    /// </summary>
    protected virtual void ApplyMainColor()
    {
        if (imgTitleUnderLine != null)
            imgTitleUnderLine.color = mainColor;

        if (imgFrameLine != null)
            imgFrameLine.color = mainColor;
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        ApplyMainColor();
    }
#endif
}
