﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogSimpleText : DialogBase
{
    [Header("Simple Text Settings")]
    [SerializeField] private Text txtNormalDescription = default;
    [SerializeField] private Text txtScrollDescription = default;
    [SerializeField] private Button btnClose = default;

    private string descpription = default;
    public string Description
    {
        get => descpription;
        set
        {
            txtNormalDescription.text = txtScrollDescription.text = descpription = value;
        }
    }

    public void SetScrollAcrtive(bool isActive)
    {
        rtScrollContent.gameObject.SetActive(isActive);
        rtNormalContent.gameObject.SetActive(!isActive);
    }

    private void Awake()
    {
        btnClose.onClick.AddListener(CloseDialog);
    }
}
