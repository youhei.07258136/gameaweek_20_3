﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.UI;

[CanEditMultipleObjects, CustomEditor(typeof(SeButton), true)]
public class SeButtonEditor : ButtonEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        this.serializedObject.Update();
        EditorGUILayout.PropertyField(this.serializedObject.FindProperty("seType"), true);
        EditorGUILayout.PropertyField(this.serializedObject.FindProperty("cueName"), true);

        this.serializedObject.ApplyModifiedProperties();
    }
}
