﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// 継承してやると色々便利なuGUI用のBehaviour
/// </summary>
public class UGUIBehaviour : MonoBehaviour
{
    private RectTransform _rectTransform;
    public RectTransform RectTransform { get => _rectTransform; }

    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        _rectTransform = GetComponent<RectTransform>();
    }
#endif
}
