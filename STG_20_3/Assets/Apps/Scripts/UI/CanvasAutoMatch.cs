﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// CanvasScalerのMatch値をいい感じにしてくれる
/// </summary>
[RequireComponent(typeof(CanvasScaler))]
[DisallowMultipleComponent]
public class CanvasAutoMatch : MonoBehaviour
{
    [SerializeField] private CanvasScaler canvasScaler = default;   //!< 対象

    private void Awake()
    {
        CalcMatchWidthOrHeight();
    }

    /// <summary>
    /// アスペクト比見てmatchの値をいい感じにする
    /// </summary>
    private void CalcMatchWidthOrHeight()
    {
        float screenAspect = (float)Screen.width / (float)Screen.height;
        float canvasAspect = canvasScaler.referenceResolution.x / canvasScaler.referenceResolution.y;

        if (screenAspect >= canvasAspect)
            canvasScaler.matchWidthOrHeight = 0.0f;
        else
            canvasScaler.matchWidthOrHeight = 1.0f;
    }

#if UNITY_EDITOR
    private Vector2Int prevScreenSize = Vector2Int.zero;

    private void Update()
    {
        if(Screen.width != prevScreenSize.x || Screen.height != prevScreenSize.y)
        {
            CalcMatchWidthOrHeight();
            prevScreenSize.x = Screen.width;
            prevScreenSize.y = Screen.height;
        }
    }
#endif
}
