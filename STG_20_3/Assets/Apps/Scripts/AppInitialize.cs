﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx.Async;
using DG.Tweening;
using UnityEngine.Scripting;

/// <summary>
/// 起動時の初期化処理
/// </summary>
public static class GameInitialize
{
    /// <summary>
    /// シーン読込直前
    /// </summary>
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void InitializeBefor()
    {
        // 最初に読み込むPrefub類
        GameObject[] objs = Resources.LoadAll<GameObject>("RuntimeInitialize");
        if (objs != null)
        {
            foreach (GameObject obj in objs)
            {
                Object.Instantiate(obj);
            }
        }

        // ここでセーブデータも読み込んでしまう
        AppSaveData.Load();

        QualitySettings.vSyncCount = 0;				// VSyncをOFFにする
        Application.targetFrameRate = 60;			// ターゲットフレームレートを60に設定
        SoundManager.Instance.SetBgmVolume(0.5f);
    }

    /// <summary>
    /// シーン読込後
    /// </summary>
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    private static async void InitializeAfter()
    {
        DOTween.SetTweensCapacity(3200, 50);

        // 最初のシーンの初期化
        await UniTask.Run(() => { SceneTransitionManager.Instance.InitializeScene(); });
    }
}
