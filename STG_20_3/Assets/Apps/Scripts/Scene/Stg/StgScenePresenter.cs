﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UniRx.Async;
using UniRx;
using UniRx.Triggers;

[RequireComponent(typeof(PhotonView))]
public class StgScenePresenter : SceneBehaviour, IConnectionCallbacks, IMatchmakingCallbacks, IInRoomCallbacks
{
    [Header("General")]
    [SerializeField] private StgSceneView sceneView = default;
    [SerializeField] private StgSceneModel sceneModel = null;
    [SerializeField] private Camera gameCamera = default;
    [SerializeField] private PhotonView photonView = default;
   
    [Header("Player")]
    [SerializeField] private Transform trPlayer = default;
    [SerializeField] private StgPlayerController prefubPlayer = default;

    [Header("Enemy")]
    [SerializeField] private Transform trEnemy = default;
    [SerializeField] private StgEnemyControllerBase[] prefubEnemys = default;

    private StgEnemyControllerBase[] enemyPool = default;
    private StgEnemyControllerBase currentEnemy = null;
    private bool isMasterSetupReady = false;

    internal override async UniTask OnChangeSceneAwake(object obj)
    {
        await base.OnChangeSceneAwake(obj);

        // Create EnemyPool
        enemyPool = new StgEnemyControllerBase[prefubEnemys.Length];
		for(int i = 0; i < prefubEnemys.Length; ++i)
		{
            StgEnemyControllerBase enemy = Instantiate(prefubEnemys[i], trEnemy);
            enemy.gameObject.SetActive(false);
            enemyPool[i] = enemy;
		}

        // Setup UI
        sceneView.SetFade(1.0f);
        sceneModel.Score.Subscribe(_ => sceneView.SetSccore(sceneModel.Score.Value));
        sceneModel.PlayerLife.Subscribe(_ => OnLifeChanged());

        // 自動スリープを無効にする
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    internal override async UniTask OnChangeSceneStart()
    {
        await base.OnChangeSceneStart();

        // Photon Settings
        PhotonNetwork.OfflineMode = false;
        PhotonNetwork.SendRate = 60;
        PhotonNetwork.SerializationRate = 20;
        PhotonNetwork.KeepAliveInBackground = 2.0f;
        PhotonNetwork.ConnectUsingSettings();
    }

    internal override async UniTask OnChangeSceneEnd()
    {
        PhotonNetwork.Disconnect();
        StgBulletFactory.Instance.ReturnAll();
        for (int i = 0; i < trPlayer.childCount; ++i)
        {
            Destroy(trPlayer.GetChild(i).gameObject);
        }
        SoundManager.Instance.StopBgm();

        await base.OnChangeSceneEnd();
    }

    internal override async UniTask OnChangeSceneDestroy()
    {
        await base.OnChangeSceneDestroy();

        // スリープ デフォルトの設定にする
        Screen.sleepTimeout = SleepTimeout.SystemSetting;
    }

    private void OnEnable()
    {
        // コールバック対象としてこのクラスを登録する
        PhotonNetwork.AddCallbackTarget(this);
    }

    private void OnDisable()
    {
        // コールバック対象の登録を解除する
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    private void OnLifeChanged()
    {
        if (sceneModel.PlayerLife.Value >= 0)
        {
            sceneView.SetPlayerLife(sceneModel.PlayerLife.Value);
        }
        else
        {
            GameOver();
        }
    }

    private async void SetupGame()
    {
        // Room create or join wait.
        await UniTask.WaitWhile(() => { return PhotonNetwork.CurrentRoom == null; });

        // 初期状態まで戻す
        for (int i = 0; i < trPlayer.childCount; ++i)
        {
            Destroy(trPlayer.GetChild(i).gameObject);
        }
        StgBulletFactory.Instance.ReturnAll();
        if (currentEnemy != null) currentEnemy.gameObject.SetActive(false);

        // Master client wait.
        isMasterSetupReady = false;
        if (PhotonNetwork.IsMasterClient)
        {
            sceneView.StartFade(1.0f, MasterSetupReady);
        }
        await UniTask.WaitWhile(() => { return !isMasterSetupReady; });

        SoundManager.Instance.PlayBgm("stg_bgm_00");

        // Fade.
        sceneView.StartFade(0.0f);

        // Create player.
        CreatePlayer();
        await UniTask.WaitWhile(() => { return PhotonNetwork.CurrentRoom.PlayerCount != trPlayer.childCount; });

        StartGame(true);
    }

    #region RPC Call
    private void StartGame(bool isSyncStart = false)
	{
        if (isSyncStart)
            sceneModel.SyncValues();
        else
            sceneModel.AddStage();

        if(PhotonNetwork.IsMasterClient)
            photonView.RPC("StartGameRPC", RpcTarget.AllViaServer);
    }

    private void CreatePlayer()
    {
        object[] args = new object[]
        {
            PhotonNetwork.AllocateViewID(false),
        };
 
        photonView.RPC("CreatePlayerRPC", RpcTarget.AllViaServer, args);
    }

	private void CreateEnemy(int stageID)
	{
        sceneModel.CalcEnemyLife();

        if (!PhotonNetwork.IsMasterClient)
            return;

        object[] args = new object[]
        {
            PhotonNetwork.AllocateViewID(false),
			stageID,
        };

        photonView.RPC("CreateEnemyRPC", RpcTarget.AllViaServer, args);
	}

    private void MasterSetupReady()
    {
        photonView.RPC("MasterSetupReadyRPC", RpcTarget.AllViaServer);
    }

    private void EnemyBulletClear()
    {
        photonView.RPC("EnemyBulletClearRPC", RpcTarget.AllViaServer);
    }

    private void GameOver()
    {
        photonView.RPC("GameOverRPC", RpcTarget.AllViaServer);
    }
    #endregion

    #region RPC
    [PunRPC]
    private void CreatePlayerRPC(int viewID)
    {
        StgPlayerController stgPlayerController = Instantiate(prefubPlayer, trPlayer);
        stgPlayerController.Initialize(
            viewID, 
            gameCamera, 
            ()=> { return sceneModel.PlayerLife.Value;  },
            ()=> { sceneModel.SubPlayerLife(); EnemyBulletClear(); SoundManager.Instance.PlaySeOneShot("stg_se_01"); }
            );
    }

	[PunRPC]
	private void CreateEnemyRPC(int viewID, int stageNum)
	{
        int index = (stageNum-1) % enemyPool.Length;
        StgEnemyControllerBase enemy = enemyPool[index];

        List<Transform> players = new List<Transform>();
        for (int i = 0; i < trPlayer.childCount; ++i)
        {
            players.Add(trPlayer.GetChild(i));
        }
        if(players.Count == 1)
        {
            players.Add(trPlayer.GetChild(0));
        }

        enemy.gameObject.SetActive(true);
        enemy.Setup(
			viewID,
            players,
			() => { return sceneModel.EnemyLife.Value; },
            () => { return sceneModel.Stage.Value; },
			() => { sceneModel.AddScore(65 * sceneModel.Stage.Value); sceneModel.SubEnemyLife(); },
			() => { StartGame(); StgBulletFactory.Instance.Return(StgBulletController.FromType.Enemy); SoundManager.Instance.PlaySeOneShot("stg_se_02"); }
			);
        currentEnemy = enemy;
        sceneView.StartEnemyLifeGauge(() => { return sceneModel.EnemyLifeRatio; });
	}

    [PunRPC]
    private void StartGameRPC()
    {
        sceneView.PlayStartAnimation(sceneModel.Stage.Value, () =>
        {
            CreateEnemy(sceneModel.Stage.Value);
        });
    }

    [PunRPC]
    private void MasterSetupReadyRPC()
    {
        isMasterSetupReady = true;
    }

    [PunRPC]
    private void EnemyBulletClearRPC()
    {
        StgBulletFactory.Instance.Return(StgBulletController.FromType.Enemy);
    }

    [PunRPC]
    private void GameOverRPC()
    {
        // これ以上進行できないので、この部屋は隠す
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;
            PhotonNetwork.CurrentRoom.IsVisible = false;
        }

        // この時点のスコアを記録
        AppSaveData.AddScore(sceneModel.Score.Value);
        AppSaveData.Save();

        // 見た目上消す
        for (int i = 0; i < trPlayer.childCount; ++i)
        {
            trPlayer.GetChild(i).gameObject.SetActive(false);
        }

        // アニメーション再生
        sceneView.PlayEndAnimation(() => { sceneView.ActiveBackToTitleButton(() => { SceneTransitionManager.Instance.ChangeScene(0); }); });
    }
    #endregion

    #region IConnectionCallbacks
    void IConnectionCallbacks.OnConnectedToMaster()
    {
        sceneView.AddSystemLog("ルームへの入室を試みています...。");
        PhotonNetwork.JoinRandomRoom();
    }

    void IConnectionCallbacks.OnDisconnected(DisconnectCause cause)
    {
        if (sceneModel.PlayerLife.Value < 0)
            return;

        sceneView.AddSystemLog("切断されました。再接続を試みます。");
        PhotonNetwork.ConnectUsingSettings();
    }

    void IConnectionCallbacks.OnConnected() { sceneView.AddSystemLog("OnConected"); }
    void IConnectionCallbacks.OnCustomAuthenticationFailed(string debugMessage) { sceneView.AddSystemLog("OnCustomAuthenticationFailed"); }
    void IConnectionCallbacks.OnCustomAuthenticationResponse(Dictionary<string, object> data) { sceneView.AddSystemLog("OnCustomAuthenticationResponse"); }
    void IConnectionCallbacks.OnRegionListReceived(RegionHandler regionHandler) { sceneView.AddSystemLog("OnRegionListReceived"); }
    #endregion

    #region IMatchmakingCallbacks
    void IMatchmakingCallbacks.OnJoinRandomFailed(short returnCode, string message)
    {
        sceneView.AddSystemLog("新規にルームを作成します...。");
        PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = 2, CleanupCacheOnLeave = false, });
    }

    void IMatchmakingCallbacks.OnJoinedRoom()
    {
        sceneView.AddSystemLog("入室しました｡");
        isMasterSetupReady = true;
        SetupGame();
    }

    void IMatchmakingCallbacks.OnCreateRoomFailed(short returnCode, string message)
    {
        sceneView.AddSystemLog("ルームの作成に失敗しました。(オフラインモード)");
        isMasterSetupReady = true;
        PhotonNetwork.OfflineMode = true;
        SetupGame();
        
    }

    void IMatchmakingCallbacks.OnJoinRoomFailed(short returnCode, string message) { sceneView.AddSystemLog("OnJoinRoomFailed"); }
    void IMatchmakingCallbacks.OnFriendListUpdate(List<FriendInfo> friendList) { sceneView.AddSystemLog("OnFriendListUpdate"); }
    void IMatchmakingCallbacks.OnCreatedRoom() { sceneView.AddSystemLog("OnCreateRoom"); }
    void IMatchmakingCallbacks.OnLeftRoom() { sceneView.AddSystemLog("OnLeftRoom"); }
    #endregion

    #region IInRoomCallbacks
    void IInRoomCallbacks.OnPlayerEnteredRoom(Player newPlayer)
    {
        sceneView.AddSystemLog("新たなプレイヤーが入室しました｡");
        SetupGame();
    }

    void IInRoomCallbacks.OnPlayerLeftRoom(Player otherPlayer)
    {
        sceneView.AddSystemLog("他のプレイヤーが離脱しました。");
        SetupGame();
    }

    void IInRoomCallbacks.OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged) { }
    void IInRoomCallbacks.OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps) { }
    void IInRoomCallbacks.OnMasterClientSwitched(Player newMasterClient) { }
    #endregion
}
