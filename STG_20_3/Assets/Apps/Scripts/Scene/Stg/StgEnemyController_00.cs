﻿using UnityEngine;
using System.Collections;
using UniRx;
using System;
using UniRx.Triggers;
using DG.Tweening;

public class StgEnemyController_00 : StgEnemyControllerBase
{
    IDisposable disposableShot;

    protected override IEnumerator Process()
	{
        // 初回登場
        mainCollider.enabled = false;
		Vector3 startPosition = new Vector3(0, 1000);
        transform.localPosition = startPosition;
		transform.DOLocalMove(new Vector3(0, 400), 3.0f).SetEase(Ease.OutCirc).SetAutoKill(true);
		yield return new WaitForSeconds(3.0f);
        mainCollider.enabled = true;

        int way = StageNum / 5 + 1;
        if (way > 36) way = 36;
        float space = 20 + (way / 5.0f);
        float time = 8 - ((StageNum - 1) * 0.05f);
        if (time < 2) time = 2;

        int playerShotTime = 3000 - (StageNum * 5);
        if (playerShotTime <= 1000)
            playerShotTime = 1000;
        disposableShot = Observable.Interval(TimeSpan.FromMilliseconds(playerShotTime)).Subscribe(_ => UpdateShot()).AddTo(this);

        // 動きながら弾発車
        while (true)
        {
			// 右
            transform.DOLocalMove(new Vector3(300, 300), 4.0f).SetEase(Ease.OutCirc).SetAutoKill(true);

            ShotWay(StgBulletController.BulletType.CirclePink_M, way, space, MainShotPos(), 180, time);
            yield return new WaitForSeconds(0.5f);                          
            ShotWay(StgBulletController.BulletType.CircleBlue_M, way, space, MainShotPos(), 180, time);
            yield return new WaitForSeconds(0.5f);                       
            ShotWay(StgBulletController.BulletType.CirclePink_M, way, space, MainShotPos(), 180, time);
            yield return new WaitForSeconds(0.5f);                          
            ShotWay(StgBulletController.BulletType.CircleBlue_M, way, space, MainShotPos(), 180, time);
            yield return new WaitForSeconds(1.5f);

			// 左
            transform.DOLocalMove(new Vector3(-300, 300), 4.0f).SetEase(Ease.OutCirc).SetAutoKill(true);

            ShotWay(StgBulletController.BulletType.CirclePink_M, way, space, MainShotPos(), 180, time);
            yield return new WaitForSeconds(0.5f);                           
            ShotWay(StgBulletController.BulletType.CircleBlue_M, way, space, MainShotPos(), 180, time);
            yield return new WaitForSeconds(0.5f);                         
            ShotWay(StgBulletController.BulletType.CirclePink_M, way, space, MainShotPos(), 180, time);
            yield return new WaitForSeconds(0.5f);                         
            ShotWay(StgBulletController.BulletType.CircleBlue_M, way, space, MainShotPos(), 180, time);
            yield return new WaitForSeconds(1.5f);
        }
    }

    protected override void OnDead()
    {
        base.OnDead();

        disposableShot?.Dispose();
    }

    private Vector3 MainShotPos()
    {
        Vector3 shotPosition = transform.localPosition;
        shotPosition.y -= 100;

        return shotPosition;
    }

    private void UpdateShot()
    {
        Vector3 shotPosL = transform.position + new Vector3(-64, -200, 0);
        Vector3 shotPosR = transform.position + new Vector3( 64, -200, 0);

        ShotWay(StgBulletController.BulletType.CircleBlue_L, 2, 20, shotPosL, AppUtil.PostToDeg(GetLeftPlayerTransform().localPosition - shotPosL), 3.0f);
        ShotWay(StgBulletController.BulletType.CirclePink_L, 2, 20, shotPosR, AppUtil.PostToDeg(GetLeftPlayerTransform().localPosition - shotPosR), 3.0f);
    }
}
