﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Photon.Pun;
using Photon.Realtime;

public class StgSceneModel : MonoBehaviourPunCallbacks 
{
	private ReactiveProperty<long> score = new ReactiveProperty<long>(0);
	private ReactiveProperty<int> stage = new ReactiveProperty<int>(1);
	private ReactiveProperty<int> playerLife = new ReactiveProperty<int>(5);
	private ReactiveProperty<int> playerBomb = new ReactiveProperty<int>(5);
    private ReactiveProperty<int> enemyLife = new ReactiveProperty<int>(1);

    public IReadOnlyReactiveProperty<long> Score { get => score; }
    public IReadOnlyReactiveProperty<int> Stage { get => stage; }
    public IReadOnlyReactiveProperty<int> PlayerLife { get => playerLife; }
	public IReadOnlyReactiveProperty<int> PlayerBomb { get => playerBomb; }
    public IReadOnlyReactiveProperty<int> EnemyLife { get => enemyLife; }

    public int EnemyLifeMax { get; private set; }
    public float EnemyLifeRatio { get { return EnemyLifeMax == 0 ? 0.0f : (float)enemyLife.Value / EnemyLifeMax; } }

    private long stageStartScore = 0;
    private int stageStartPlayerLife = 5;

	public void SubPlayerLife()
	{
        photonView.RPC("SubPlayerLifeRPC", RpcTarget.All);
    }

    public void AddPlayerLife()
    {
        if (!PhotonNetwork.IsMasterClient)
            return;

        photonView.RPC("AddPlayerLifeRPC", RpcTarget.All);
    }

    public void SubPlayerBomb(int sub)
	{
        photonView.RPC("SubPlayerBombRPC", RpcTarget.All, sub);
    }

    public void CalcEnemyLife()
    {
        if (!PhotonNetwork.IsMasterClient)
            return;

        photonView.RPC("CalcEnemyLifeRPC", RpcTarget.All);
    }

    public void SubEnemyLife()
    {
        photonView.RPC("SubEnemyLifeRPC", RpcTarget.All);
    }

    public void AddScore(long addScore)
	{
        photonView.RPC("AddScoreRPC", RpcTarget.All, addScore);
    }

    public void AddStage()
    {
        if (!PhotonNetwork.IsMasterClient)
            return;

        photonView.RPC("AddStageRPC", RpcTarget.All);
    }

    public void SyncValues()
    {
        if (!PhotonNetwork.IsMasterClient)
            return;

        playerLife.Value = stageStartPlayerLife;
        score.Value = stageStartScore;
        enemyLife.Value = EnemyLifeMax;

        object[] args = new object[]
        {
            Score.Value,
            stageStartScore,
            Stage.Value,
            PlayerLife.Value,
            PlayerBomb.Value
        };

        photonView.RPC("SyncValuesRPC", RpcTarget.All, args);
    }

    [PunRPC]
    private void CalcEnemyLifeRPC()
    {
        enemyLife.Value = 900 + (int)(stage.Value * 7.5f + stage.Value);
        EnemyLifeMax = enemyLife.Value;
    }

    [PunRPC]
    private void SubPlayerLifeRPC()
    {
        playerLife.Value -= 1;
    }

    [PunRPC]
    private void AddPlayerLifeRPC()
    {
        playerLife.Value += 1;
    }

    [PunRPC]
    private void SubPlayerBombRPC(int sub)
    {
        if (playerBomb.Value - sub <= 0)
            playerBomb.Value = 0;
        else
            playerBomb.Value -= sub;
    }

    [PunRPC]
    private void SubEnemyLifeRPC()
    {
        int val = PhotonNetwork.CurrentRoom.PlayerCount == 2 ? 2 : 3;

        if (enemyLife.Value - val <= 0)
            enemyLife.Value = 0;
        else
            enemyLife.Value -= val;
    }

    [PunRPC]
    private void AddScoreRPC(long addScore)
    {
        if (score.Value + addScore > AppDefine.StgScoreMax)
            score.Value = AppDefine.StgScoreMax;
        else
            score.Value += addScore;
    }

    [PunRPC]
    private void AddStageRPC()
    {
        stageStartPlayerLife = playerLife.Value;
        stageStartScore = Score.Value;
        stage.Value++;

        if (stage.Value % 10 == 0)
            AddPlayerLife();
    }

    [PunRPC]
    private void SyncValuesRPC(long score, long stageStartScore, int stage, int playerLife, int playerBomb)
    {
        this.score.Value = score;
        this.stageStartScore = stageStartScore;
        this.stage.Value = stage;
        this.playerLife.Value = playerLife;
        this.playerBomb.Value = playerBomb;
    }
}
