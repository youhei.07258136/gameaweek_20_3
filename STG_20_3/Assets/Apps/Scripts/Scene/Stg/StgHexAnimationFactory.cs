﻿using UnityEngine;
using System.Collections;
using System;
using UniRx;
using UniRx.Async;

public class StgHexAnimationFactory : SingletonSceneBehaviour<StgHexAnimationFactory>
{
    [SerializeField] private GameObject prefabHexAnimation = default;
    [SerializeField] private Transform trParent = default;

    private StgHexAnimationPool animationPool = null;

    internal override async UniTask OnChangeSceneAwake(object obj)
    {
        await base.OnChangeSceneAwake(obj);

        animationPool = new StgHexAnimationPool(prefabHexAnimation, trParent);
        IObservable<Unit> observable = animationPool.PreloadAsync(768, 768);
        observable.Subscribe();
        await observable;
    }

    internal override async UniTask OnChangeSceneDestroy()
    {
        await base.OnChangeSceneDestroy();

        await AppUtil.ReleasePoolAsync(animationPool);
    }

    public StgHexAnimationController Rent(Vector3 pos)
    {
        StgHexAnimationController ret = animationPool.Rent();
        ret.transform.localPosition = pos;
        return ret;
    }

    public void Return(StgHexAnimationController anim)
    {
        animationPool.Return(anim);
    }
}
