﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UniRx.Async;

public class StgEnemyLifeGauge : UGUIBehaviour
{
    [SerializeField] private Image imgGauge = default;
	[SerializeField] private CanvasGroup canvasGroup = default;
	private Vector2 gaugeSize = default;

	private void Awake()
	{
		gaugeSize = imgGauge.rectTransform.sizeDelta;
		imgGauge.rectTransform.sizeDelta = new Vector2(0, gaugeSize.y);
		canvasGroup.alpha = 0;
	}

	public async void Setup(Func<float> lifeRatioCallback)
	{
		canvasGroup.alpha = 0;
		canvasGroup.DOKill(true);
		if (lifeRatioCallback == null)
			return;

		// Begin
		canvasGroup
			.DOFade(1.0f, 0.25f)
			.SetEase(Ease.InCirc)
			.SetAutoKill(true);

		// Loop
		await UniTask.WaitWhile(() =>
		{
            if (imgGauge == null)
                return true;

			float ratio = lifeRatioCallback();
			if (ratio < 0) ratio = 0;

			imgGauge.rectTransform.sizeDelta = new Vector2(gaugeSize.x * ratio, gaugeSize.y);
			if (ratio <= 0)
				return false;

			return true;
		});

		// End
		canvasGroup
			.DOFade(0.0f, 0.25f)
			.SetEase(Ease.InCirc)
			.SetAutoKill(true);
	}
}
