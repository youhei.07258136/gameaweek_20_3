﻿using UnityEngine;
using System.Collections;
using UniRx;
using System;
using UniRx.Triggers;
using DG.Tweening;

public class StgEnemyController_03 : StgEnemyControllerBase
{
    private static readonly Vector3 BasePos = new Vector3(0, 0);

    IDisposable disposableMove;
    IDisposable disposableShot;

    float moves = Mathf.PI * 2;
    float shotRotate = 0;

    protected override IEnumerator Process()
    {
        disposableShot = null;

        mainCollider.enabled = false;
        Vector3 startPosition = new Vector3(0, 1000);
        transform.localPosition = startPosition;
        transform.DOLocalMove(BasePos, 3.0f).SetEase(Ease.OutCirc).SetAutoKill(true);
        yield return new WaitForSeconds(3.0f);

        mainCollider.enabled = true;

        moves = Mathf.PI * 2;
        shotRotate = 0;
        disposableMove = this.UpdateAsObservable().Subscribe(_ => UpdateMove()).AddTo(this);
        disposableShot = Observable.Interval(TimeSpan.FromMilliseconds(128)).Subscribe(_ => UpdateShot()).AddTo(this);

        float wait = 3.0f - (StageNum * 0.035f);
        if (wait < 0.48f)
            wait = 0.48f;
        
        while (true)
        {
            Vector3 shotPosL = transform.position + new Vector3(-128, -128, 0);
            Vector3 shotPosR = transform.position + new Vector3( 128, -128, 0);

            // 時機狙い
            ShotWay(StgBulletController.BulletType.CirclePink_L, 1, 0, shotPosL, AppUtil.PostToDeg(GetLeftPlayerTransform().localPosition - shotPosL), 5);
            ShotWay(StgBulletController.BulletType.CirclePink_L, 1, 0, shotPosR, AppUtil.PostToDeg(GetRightPlayerTransform().localPosition - shotPosR), 5);

            yield return new WaitForSeconds(wait);
        }
    }

    protected override void OnDead()
    {
        base.OnDead();

        disposableShot?.Dispose();
        disposableMove?.Dispose();
    }

    private void UpdateMove()
    {
        // 8の字に動かす
        Vector3 pos = transform.localPosition;
        moves += Mathf.PI * Time.deltaTime;
        pos.x = BasePos.x + 64 * Mathf.Cos(moves * 0.25f);
        pos.y = BasePos.y + 32 * Mathf.Sin(moves * 0.25f * 2);
        transform.localPosition = pos;
    }

    private void UpdateShot()
    {
        Vector3 shotPosL = transform.position + new Vector3(-8, 8, 0);
        Vector3 shotPosR = transform.position + new Vector3(8, 8, 0);

        float rotateSpped = (StageNum * 0.076f);
        if (rotateSpped > 3.2f)
            rotateSpped = 3.2f;

        shotRotate += rotateSpped;

        ShotWay(StgBulletController.BulletType.CircleBlue_S, 1, 0, shotPosL,  -45 + shotRotate, 4.0f);
        ShotWay(StgBulletController.BulletType.CircleBlue_S, 1, 0, shotPosL, -135 + shotRotate, 4.0f);
        ShotWay(StgBulletController.BulletType.CircleBlue_S, 1, 0, shotPosR,   45 + shotRotate, 4.0f);
        ShotWay(StgBulletController.BulletType.CircleBlue_S, 1, 0, shotPosR,  135 + shotRotate, 4.0f);
    }
}
