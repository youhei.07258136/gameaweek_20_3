﻿using UnityEngine;
using System.Collections;
using UniRx;
using System;
using UniRx.Triggers;
using DG.Tweening;

public class StgEnemyController_01 : StgEnemyControllerBase
{
    float moves = Mathf.PI * 2;
    IDisposable disposableMove;

    private static readonly Vector3 BasePos = new Vector3(0, 400);

    protected override IEnumerator Process()
	{
        // 初回登場
        mainCollider.enabled = false;

        Vector3 startPosition = new Vector3(0, 1000);
		transform.localPosition = startPosition;
		transform.DOLocalMove(new Vector3(0, 400), 3.0f).SetEase(Ease.OutCirc).SetAutoKill(true);
		yield return new WaitForSeconds(3.0f);
        mainCollider.enabled = true;

        moves = Mathf.PI * 2;
        disposableMove = this.UpdateAsObservable().Subscribe(_ => UpdateMove()).AddTo(this);

        float time = 5.0f - ((float)StageNum * 0.03f);
        float wait = 0.8f - ((float)StageNum * 0.02f);

        if (time < 2.5f) time = 2.5f;
        if (wait < 0.064f) wait = 0.064f;

        int cnt = 0;

        float rot = StageNum >= 20 ? 120 : 180;
		while (true)
		{
            Vector3 shotPosition = transform.localPosition;
            shotPosition.y -= 100;

            ShotWay(StgBulletController.BulletType.RhombusPink, 1, 0, shotPosition, rot, time);
            ShotWay(StgBulletController.BulletType.RhombusPink, 1, 0, shotPosition, rot+180, time);

            if (StageNum >= 20)
            {
                ShotWay(StgBulletController.BulletType.RhombusBlue, 1, 0, shotPosition, -rot, time);
                ShotWay(StgBulletController.BulletType.RhombusBlue, 1, 0, shotPosition, -rot+180, time);
            }

            if(cnt % 20 == 0)
            {
                // 時機狙い
                Vector3 shotPosL = transform.position + new Vector3(-128, -128, 0);
                Vector3 shotPosR = transform.position + new Vector3(128, -128, 0);
                ShotWay(StgBulletController.BulletType.CircleBlue_L, 1, 0, shotPosL, AppUtil.PostToDeg(GetLeftPlayerTransform().localPosition - shotPosL), 2);
                ShotWay(StgBulletController.BulletType.CircleBlue_L, 1, 0, shotPosR, AppUtil.PostToDeg(GetRightPlayerTransform().localPosition - shotPosR), 2);
            }

            cnt++;

            yield return new WaitForSeconds(wait);
			rot += 5;
			if (rot >= 180 + 360) rot -= 360;
		}
	}

    protected override void OnDead()
    {
        base.OnDead();
        disposableMove?.Dispose();
    }

    private void UpdateMove()
    {
        // 8の字に動かす
        Vector3 pos = transform.localPosition;
        moves += Mathf.PI * Time.deltaTime * 2;
        pos.x = BasePos.x + 64 * Mathf.Cos(moves * 0.25f);
        pos.y = BasePos.y + 32 * Mathf.Sin(moves * 0.25f * 2);
        transform.localPosition = pos;
    }
}
