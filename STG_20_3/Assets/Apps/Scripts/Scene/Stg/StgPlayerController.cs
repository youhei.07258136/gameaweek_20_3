﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.U2D;
using Photon.Pun;
using Photon.Realtime;
using UniRx;
using UniRx.Triggers;
using DG.Tweening;

/// <summary>
/// Player
/// 操作とかは管理するがパラメータは外部依存
/// </summary>
public class StgPlayerController : MonoBehaviourPunCallbacks
{
    /// <summary>
    /// 弾丸の発射情報
    /// </summary>
    private class ShotInfo
    {
        public Vector3 pos;
        public float rotateDeg;

        public ShotInfo(Vector3 p, float r)
        {
            pos = p;
            rotateDeg = r;
        }
    };

    /// <summary>
    /// 弾丸の発射情報の設定(ここに追記すれば、発射数増やせるように)
    /// </summary>
    static private readonly ShotInfo[] ShotInfos = new ShotInfo[]
    {
        // Center
        new ShotInfo(new Vector3(0, 72, 0), 0.0f),

        // L/R
        new ShotInfo(new Vector3(-36, 32, 0), 0.0f),
        new ShotInfo(new Vector3( 36, 32, 0), 0.0f),

        // L/R and rotate
        new ShotInfo(new Vector3(-48, 0, 0), -20.0f),
        new ShotInfo(new Vector3( 48, 0, 0),  20.0f),
    };

    [SerializeField] private SpriteAtlas spriteAtlas = default;
    [SerializeField] private SpriteRenderer mainSprite = default;
    [SerializeField] private SpriteRenderer hitPosSprite = default;
    [SerializeField] private CircleCollider2D circleCollider = default;
    [SerializeField] private CriAtomSource seShot = default;

    private Func<int> lifeFunc = default;
    private Action hitCallback = default;
    private Vector3 playerPos = default;
    private Vector3 mousePos = default;
    private Camera targetCamera = default;

    // いつでも各種更新キャンセルできるようにそれぞれDisposable持つことにした
    private IDisposable disposableUpdate = null;
    private IDisposable disposableShot = null;
    private IDisposable disposableEnableCollider = null;

    private int Life { get => lifeFunc == null ? 0 : lifeFunc(); }

    /// <summary>
    /// OnDisable
    /// </summary>
    public override void OnDisable()
    {
        base.OnDisable();
        Cancel();
    }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="viewID"></param>
    /// <param name="targetCamera"></param>
    /// <param name="lifeFunc"></param>
    /// <param name="hitCallback"></param>
    public void Initialize(int viewID, Camera targetCamera, Func<int> lifeFunc, Action hitCallback)
    {
        // Param set
        photonView.ViewID = viewID;
        this.targetCamera = targetCamera;
        this.hitCallback = hitCallback;
        this.lifeFunc = lifeFunc;

        // Color setting 1P / 2P
        if(photonView.IsMine)
        {
            mainSprite.sprite = spriteAtlas.GetSprite("stg_player_chara_0");
            hitPosSprite.sprite = spriteAtlas.GetSprite("stg_player_hit_pos_0");
        }
        else
        {
            mainSprite.sprite = spriteAtlas.GetSprite("stg_player_chara_1");
            hitPosSprite.sprite = spriteAtlas.GetSprite("stg_player_hit_pos_1");
        }

        seShot.Stop();
        InAnimation();
    }

    /// <summary>
    /// 出撃アニメーション再生
    /// </summary>
    private void InAnimation()
    {
        Cancel();

        Vector3 startPos;
        Vector3 endPos;

        if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
        {
            startPos = new Vector3(0, -1000);
            endPos = new Vector3(0, -400);
        }
        else if(photonView.Controller.IsMasterClient)
        {
            startPos = new Vector3(-200, -1000);
            endPos = new Vector3(-200, -400);
        }
        else
        {
            startPos = new Vector3(200, -1000);
            endPos = new Vector3(200, -400);
        }

        // 無敵中のカラー更新
        mainSprite.color = Color.white;
        mainSprite.DOFade(0, 0.036f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear).SetAutoKill(true);

        // 移動
        transform.position = startPos;
        transform.DOLocalMove(endPos, 2.0f).OnComplete(OnInAnimationComplete).SetAutoKill(true);
    }

    /// <summary>
    /// 各種処理をキャンセルする(ResetはUnityのメッセージに存在するのでさけた)
    /// </summary>
    private void Cancel()
    {
        if (disposableUpdate != null) disposableUpdate.Dispose();
        if (disposableShot != null) disposableShot.Dispose();
        if (disposableEnableCollider != null) disposableEnableCollider.Dispose();

        circleCollider.enabled = false;
        seShot.Stop();
        transform.DOKill(false);
        mainSprite.DOKill(false);
    }

    /// <summary>
    /// 出撃アニメーションが終わった
    /// </summary>
    private void OnInAnimationComplete()
    {
        // うるさいので発射音は自分だけに限定
        if (photonView.IsMine)
            seShot.Play();

        // 生存時更新開始
        disposableUpdate = this
            .UpdateAsObservable()
            .Where(_ => Life >= 0)
            .Subscribe(_ => OnAliveUpdate())
            .AddTo(this);

        // 発射更新開始
        disposableShot = Observable
            .Interval(TimeSpan.FromMilliseconds(32))
            .Subscribe(_ => { OnShooting(); })
            .AddTo(this);

        // 指定時間後に無敵解除
        disposableEnableCollider = Observable
            .Timer(TimeSpan.FromMilliseconds(1000))
            .Subscribe(_ => {
                circleCollider.enabled = true;
                mainSprite.DOKill(false);
                mainSprite.color = Color.white;
            })
            .AddTo(this);
    }

    /// <summary>
    /// 生存中の更新処理
    /// </summary>
    private void OnAliveUpdate()
    {
        if (photonView.IsMine)
            PlayerControl();
    }

    /// <summary>
    /// 弾発射時のイベント
    /// </summary>
    private void OnShooting()
    {
        foreach(ShotInfo info in ShotInfos)
        {
            Shot(info.pos, info.rotateDeg);
        }
    }
    
    /// <summary>
    /// 端末操作 (ネットのコードコピペ）
    /// </summary>
    private void PlayerControl()
    {
        Vector2 sizeRatio = new Vector2();
        float ratio = 0;
        sizeRatio.x = AppDefine.StgStageWidth / (float)Screen.width;
        sizeRatio.y = AppDefine.StgStageHeight / (float)Screen.height;

        if (Screen.width < Screen.height)
            ratio = sizeRatio.x;
        else
            ratio = sizeRatio.y;

        if (Input.GetMouseButtonDown(0))
        {
            Vector3 inputMousePoint = Vector3.zero;
            inputMousePoint.x = Input.mousePosition.x * ratio;
            inputMousePoint.y = Input.mousePosition.y * ratio;

            playerPos = this.transform.position;
            mousePos = targetCamera.ScreenToWorldPoint(inputMousePoint);
        }

        if (Input.GetMouseButton(0))
        {
            Vector3 prePos = this.transform.position;
            Vector3 diff;

            //タッチ対応デバイス向け、1本目の指にのみ反応
            if (Input.touchSupported)
            {
                Vector3 inputTouchPoint = Vector3.zero;
                inputTouchPoint.x = Input.GetTouch(0).position.x * ratio;
                inputTouchPoint.y = Input.GetTouch(0).position.y * ratio;

                diff = targetCamera.ScreenToWorldPoint(inputTouchPoint) - mousePos;
            }
            else
            {
                Vector3 inputMousePoint = Vector3.zero;
                inputMousePoint.x = Input.mousePosition.x * ratio;
                inputMousePoint.y = Input.mousePosition.y * ratio;

                diff = targetCamera.ScreenToWorldPoint(inputMousePoint) - mousePos;
            }

            diff.z = 0.0f;
            this.transform.position = playerPos + diff;

            // 移動制限
            Vector3 pos = this.transform.position;
            if (pos.x < -AppDefine.StgStageWidth/2) pos.x = -AppDefine.StgStageWidth / 2;
            else if (pos.x > AppDefine.StgStageWidth/2) pos.x = AppDefine.StgStageWidth / 2;
            if (pos.y < -AppDefine.StgStageHeight / 2) pos.y = -AppDefine.StgStageHeight / 2;
            else if (pos.y > AppDefine.StgStageHeight / 2) pos.y = AppDefine.StgStageHeight / 2;
            transform.position = pos;
        }

        if (Input.GetMouseButtonUp(0))
        {
            playerPos = Vector3.zero;
            mousePos = Vector3.zero;
        }
    }

    /// <summary>
    /// 一発分の生成(1P/2P考慮)
    /// </summary>
    /// <param name="addPos"></param>
    /// <param name="rotateDeg"></param>
    private void Shot(Vector3 addPos, float rotateDeg)
    {
        StgBulletController.FromType fromType = photonView.IsMine ? StgBulletController.FromType.Player1 : StgBulletController.FromType.Player2;
        StgBulletController.BulletType bulletType = photonView.IsMine ? StgBulletController.BulletType.RhombusBlue : StgBulletController.BulletType.RhombusPink;

        StgBulletController bullet = StgBulletFactory.Instance.Rent(fromType);
        if (bullet == null) return;

        bullet.MoveEase(bulletType, this.transform.position + addPos, rotateDeg, 0.3f);
    }

    /// <summary>
    /// OnTriggerEnter2D
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!photonView.IsMine)
            return;

        if (collision.gameObject.tag != AppDefine.StgEnemyBulletTag)
            return;

        circleCollider.enabled = false;
        hitCallback?.Invoke();

        if(Life >= 0)
            photonView.RPC("RetryRPC", RpcTarget.All); 
    }

    /// <summary>
    /// RetryRPC
    /// </summary>
    [PunRPC]
    private void RetryRPC()
    {
        StgHexAnimationFactory.Instance.Rent(transform.position).Play(Vector3.one * 1.5f);

        seShot.Stop();
        InAnimation();
    }
}
