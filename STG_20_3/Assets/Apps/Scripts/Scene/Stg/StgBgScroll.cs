﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StgBgScroll : MonoBehaviour
{
    [SerializeField] Material material0 = default;
    [SerializeField] Material material1 = default;

    void Update()
    {
        material0.SetTextureOffset("_MainTex", new Vector2(0, Mathf.Repeat(Time.time * 1.25f, 1)));
        material1.SetTextureOffset("_MainTex", new Vector2(0, Mathf.Repeat(Time.time * 0.8f, 1)));
    }
}
