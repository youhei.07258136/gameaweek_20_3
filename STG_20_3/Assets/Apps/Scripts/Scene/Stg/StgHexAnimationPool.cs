﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Toolkit;

public class StgHexAnimationPool : ObjectPool<StgHexAnimationController>
{

    private GameObject prefab = null;
    private Transform parent = null;

    public StgHexAnimationPool(GameObject prefab, Transform parent)
    {
        this.prefab = prefab;
        this.parent = parent;
    }

    protected override StgHexAnimationController CreateInstance()
    {
        StgHexAnimationController ret = Object.Instantiate(prefab).GetComponent<StgHexAnimationController>();
        ret.transform.parent = parent;
        
        return ret;
    }

    protected override void OnBeforeReturn(StgHexAnimationController instance)
    {
        base.OnBeforeReturn(instance);
        instance.transform.parent = parent;
    }
}
