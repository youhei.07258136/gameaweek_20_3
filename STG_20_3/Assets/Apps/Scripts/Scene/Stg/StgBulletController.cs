﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UniRx.Async;
using DG.Tweening;

/// <summary>
/// 弾クラス
/// アップベクトル、右回転仕様
/// </summary>
public class StgBulletController : MonoBehaviour
{
    /// <summary>
    /// 弾種別
    /// </summary>
    public enum BulletType
    {
        RhombusPink,
        RhombusBlue,
        CirclePink_S,
        CircleBlue_S,
        CirclePink_M,
        CircleBlue_M,
        CirclePink_L,
        CircleBlue_L,
        CirclePink_LL,
        CircleBlue_LL,
    }

    public enum FromType
    {
        Enemy,
        Player1,
        Player2,
    };

    /// <summary>
    /// 弾情報
    /// </summary>
    public class BulletInfo
    {
        public float collisionRadius;   //!< コリジョン半径
        public string spriteName;       //!< スプライト名
    }

    public const int BulletTypeNum = 10;
    public const int FromTypeNum = 3;
    public const float MoveDistanceDefalt = AppDefine.StgStageHeight;

    /// <summary>
    /// 弾情報の一覧(enum順に一致するように配置)
    /// </summary>
    private static readonly BulletInfo[] BulletInfos = new BulletInfo[]
    {
        new BulletInfo() { collisionRadius =  16, spriteName = "stg_bullet_00_0" },
        new BulletInfo() { collisionRadius =  16, spriteName = "stg_bullet_00_1" },
        new BulletInfo() { collisionRadius =  16, spriteName = "stg_bullet_10_0" },
        new BulletInfo() { collisionRadius =  16, spriteName = "stg_bullet_10_1" },
        new BulletInfo() { collisionRadius =  32, spriteName = "stg_bullet_11_0" },
        new BulletInfo() { collisionRadius =  32, spriteName = "stg_bullet_11_1" },
        new BulletInfo() { collisionRadius =  64, spriteName = "stg_bullet_12_0" },
        new BulletInfo() { collisionRadius =  64, spriteName = "stg_bullet_12_1" },
        new BulletInfo() { collisionRadius = 128, spriteName = "stg_bullet_13_0" },
        new BulletInfo() { collisionRadius = 128, spriteName = "stg_bullet_13_1" },
    };

    // SerializeFields
    [SerializeField] private SpriteAtlas spriteAtlas = default;
    [SerializeField] private SpriteRenderer spriteRenderer = default;
    [SerializeField] private CircleCollider2D circleCollider = default;

    public FromType From { get; private set; } = FromType.Enemy;

	private void OnDisable()
	{
        transform.DOKill(false);
    }

    public void PlayHexAnimation()
    {
        StgHexAnimationFactory.Instance.Rent(transform.position).Play(Vector3.one * (circleCollider.radius / 8));
    }

	public void SetFromType(FromType from)
    {
        From = from;
    }

    public void MoveEase(BulletType type, Vector2 beginPos, float rotateDeg, float time, Ease ease = Ease.Linear, float distance = MoveDistanceDefalt)
    {
        // 初期配置
        MoveSetup(type, beginPos, -rotateDeg);

        // 指定方向へ飛ばす
        Vector2 endPos = new Vector2();
        endPos.y = Mathf.Cos(rotateDeg * Mathf.Deg2Rad) * distance + beginPos.y;
        endPos.x = Mathf.Sin(rotateDeg * Mathf.Deg2Rad) * distance + beginPos.x;
        transform
            .DOLocalMove(endPos, time)
            .SetEase(ease)
            .OnComplete(() => { StgBulletFactory.Instance.Return(this); })
            .SetAutoKill(true);
    }

    private void MoveSetup(BulletType type, Vector2 pos, float rotate)
    {
        BulletInfo info = BulletInfos[(int)type];

        spriteRenderer.sprite = spriteAtlas.GetSprite(info.spriteName);
        circleCollider.radius = info.collisionRadius;

        transform.localPosition = pos;
        transform.localRotation = Quaternion.Euler(0, 0, rotate);
    }
}
