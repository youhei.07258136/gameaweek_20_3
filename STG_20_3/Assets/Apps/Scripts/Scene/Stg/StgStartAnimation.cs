﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UniRx.Async;

public class StgStartAnimation : UGUIBehaviour
{
    [SerializeField] private RectTransform rtRoot = default;
	[SerializeField] private Text txtWarning = default;
	[SerializeField] private Text txtStageNumber = default;

	private Color warningColor = default;
	private Color stageNumberColor = default;

	private void Awake()
	{
		//gameObject.SetActive(false);
		warningColor = txtWarning.color;
		stageNumberColor = txtStageNumber.color;
	}

	public async void Play(int stageNum, Action endCallback)
	{
		// init
		gameObject.SetActive(true);
		rtRoot.localScale = new Vector3(1, 0);
		txtWarning.color = new Color(warningColor.r, warningColor.g, warningColor.b, 0);
		txtStageNumber.color = new Color(stageNumberColor.r, stageNumberColor.g, stageNumberColor.b, 0);
		txtStageNumber.text = "STAGE\n" + stageNum.ToString();

		// begin
		rtRoot.DOScale(Vector3.one, 0.5f).SetEase(Ease.InOutCirc).SetAutoKill(true);
		txtWarning.DOColor(warningColor, 0.5f).SetEase(Ease.InOutCirc).SetAutoKill(true);
		txtStageNumber.DOColor(stageNumberColor, 0.5f).SetEase(Ease.InOutCirc).SetAutoKill(true);

		await UniTask.Delay(250);

        SoundManager.Instance.PlaySeOneShot("stg_se_04");

        await UniTask.Delay(250);

        // Loop
        txtWarning
			.DOColor(new Color(warningColor.r, warningColor.g, warningColor.b, 0), 0.25f)
			.SetEase(Ease.InOutCirc)
			.SetLoops(-1, LoopType.Yoyo)
			.SetAutoKill(true);
		await UniTask.Delay(500*5);

		// End
		rtRoot.DOScale(new Vector3(1, 0), 0.5f).SetEase(Ease.InOutCirc).SetAutoKill(true);
		txtWarning.DOColor(new Color(warningColor.r, warningColor.g, warningColor.b, 0), 0.5f).SetEase(Ease.InOutCirc).SetAutoKill(true);
		txtStageNumber.DOColor(new Color(stageNumberColor.r, stageNumberColor.g, stageNumberColor.b, 0), 0.5f).SetEase(Ease.InOutCirc).SetAutoKill(true);

		await UniTask.Delay(500);

		// destroy
		transform.DOKill(true);
		txtWarning.DOKill(true);
		txtStageNumber.DOKill(true);
		endCallback?.Invoke();
		gameObject.SetActive(false);
	}
}
