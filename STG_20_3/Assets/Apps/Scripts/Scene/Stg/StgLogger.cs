﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StgLogger : MonoBehaviour
{
    [SerializeField] private Text txtLog = default;
    [SerializeField] private int lineMax = 5;

    private List<string> logs = new List<string>();

    private void Awake()
    {
        txtLog.text = "";
    }

    public void AddSystemLog(string log)
    {
        AddLog("[SYSTEM] " + log);
        txtLog.color = Color.white;
        txtLog.text = GetLogString();
    }

    private void AddLog(string log)
    {
        logs.Add(log);

        if (logs.Count > lineMax)
            logs.RemoveAt(0);
    }

    private string GetLogString()
    {
        string log = "";

        for(int i = 0; i < logs.Count; ++i)
        {
            if (i != 0)
                log += "\n";
            log += logs[i];
        }

        return log;
    }
}
