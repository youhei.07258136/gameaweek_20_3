﻿using System.Collections;
using System.Collections.Generic;
using UniRx.Async;
using UnityEngine;
using UniRx;
using System;

public class StgBulletFactory : SingletonSceneBehaviour<StgBulletFactory>
{
    private const int EnemyBulletsPoolNum = 512;
    private const int PlayerBulletsPoolNum = 512;


    [SerializeField] private GameObject prefabBullet = default;
    [SerializeField] private Transform enemyBulletsTransform = default;
    [SerializeField] private Transform playerBulletsTransform = default;

    private StgBulletPool enemyBullletsPool = null;
    private StgBulletPool playerBullletsPool = null;
    private List<StgBulletController>[] activeBullets = null;

    internal override async UniTask OnChangeSceneAwake(object obj)
    {
        await base.OnChangeSceneAwake(obj);

        // 有効弾管理用
        activeBullets = new List<StgBulletController>[StgBulletController.FromTypeNum]
        {
            new List<StgBulletController>(),
            new List<StgBulletController>(),
            new List<StgBulletController>(),
        };

        // プール生成
        enemyBullletsPool  = await CreateBulletsPool(enemyBulletsTransform, EnemyBulletsPoolNum);
        playerBullletsPool = await CreateBulletsPool(playerBulletsTransform, PlayerBulletsPoolNum);
    }

    internal override async UniTask OnChangeSceneDestroy()
    {
        await base.OnChangeSceneDestroy();

        for (int i = 0; i < activeBullets.Length; ++i)
        {
            activeBullets[i].Clear();
        }

        await AppUtil.ReleasePoolAsync(enemyBullletsPool);
        await AppUtil.ReleasePoolAsync(playerBullletsPool);
    }

    public StgBulletController Rent(StgBulletController.FromType from)
    {
        StgBulletController ret = from == StgBulletController.FromType.Enemy ? enemyBullletsPool.Rent() : playerBullletsPool.Rent();
        
        switch(from)
        {
            case StgBulletController.FromType.Enemy:    ret.tag = AppDefine.StgEnemyBulletTag; break;
            case StgBulletController.FromType.Player1:  ret.tag = AppDefine.StgPlayerMineBulletTag; break;
            case StgBulletController.FromType.Player2:  ret.tag = AppDefine.StgPlayerOtherBulletTag; break;
        }
        
        ret.SetFromType(from);
        activeBullets[(int)from].Add(ret);

        return ret;
    }

    public void Return(StgBulletController bullet)
    {
        if (activeBullets == null)
            return;
        if (bullet == null)
            return;

        activeBullets[(int)bullet.From].Remove(bullet);

        if (bullet.From == StgBulletController.FromType.Enemy)
            enemyBullletsPool.Return(bullet);
        else
            playerBullletsPool.Return(bullet);
    }

    public void Return(StgBulletController.FromType from)
    {
        List<StgBulletController> list = activeBullets[(int)from];

        for(int i = 0; i < list.Count; ++i)
        {
            if (from == StgBulletController.FromType.Enemy)
            {
                list[i].PlayHexAnimation();
                enemyBullletsPool.Return(list[i]);
            }
            else
                playerBullletsPool.Return(list[i]);
        }
        activeBullets[(int)from].Clear();
    }

    public void ReturnAll()
    {
        Return(StgBulletController.FromType.Enemy);
        Return(StgBulletController.FromType.Player1);
        Return(StgBulletController.FromType.Player2);

    }

    private async UniTask<StgBulletPool> CreateBulletsPool(Transform parent, int poolNum)
    {
        StgBulletPool pool = new StgBulletPool(prefabBullet, parent);
        IObservable<Unit> observable = pool.PreloadAsync(poolNum, poolNum / 10);
        observable.Subscribe();
        await observable;

        return pool;
    }
}