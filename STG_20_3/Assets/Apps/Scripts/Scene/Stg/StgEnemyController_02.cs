﻿using UnityEngine;
using System.Collections;
using UniRx;
using System;
using UniRx.Triggers;
using DG.Tweening;

public class StgEnemyController_02 : StgEnemyControllerBase
{
    private static readonly Vector3 BasePos = new Vector3(0, 400);

    [SerializeField] private Transform trRoot = default;
    [SerializeField] private Transform[] trShotPos = default;

    IDisposable disposableRoate;
    IDisposable disposableMove;
    IDisposable disposableShot;

    float moves = Mathf.PI * 2;
    int counter = 0;

    protected override IEnumerator Process()
    {
        disposableRoate = this.UpdateAsObservable().Subscribe(_ => UpdateRotate()).AddTo(this);

        mainCollider.enabled = false;
        Vector3 startPosition = new Vector3(0, 1000);
        transform.localPosition = startPosition;
        transform.DOLocalMove(BasePos, 3.0f).SetEase(Ease.OutCirc).SetAutoKill(true);
        yield return new WaitForSeconds(3.0f);

        mainCollider.enabled = true;
        moves = Mathf.PI * 2;
        counter = 0;
        disposableMove = this.UpdateAsObservable().Subscribe(_ => UpdateMove()).AddTo(this);
        UpdateShot();
        disposableShot = Observable.Interval(TimeSpan.FromMilliseconds(5000)).Subscribe(_ => UpdateShot()).AddTo(this);

        float wait = 1.0f - (StageNum * 0.016f);
        if (wait < 0.032f)
            wait = 0.032f;

        while (true)
        {
            for (int i = 0; i < trShotPos.Length; ++i)
            {
                Transform tr = trShotPos[i];
                
                float shotRot = i < trShotPos.Length / 2 ?
                    -tr.rotation.eulerAngles.z + tr.localRotation.eulerAngles.z + 90 :
                    -tr.rotation.eulerAngles.z + tr.localRotation.eulerAngles.z - 90;
                StgBulletController.BulletType bulletType = i < trShotPos.Length / 2 ? StgBulletController.BulletType.CircleBlue_M : StgBulletController.BulletType.CirclePink_S;

                ShotWay(bulletType, 1, 0, tr.position, shotRot, 3);
            }

            yield return new WaitForSeconds(wait);
        }
    }

    protected override void OnDead()
    {
        base.OnDead();

        disposableRoate?.Dispose();
        disposableMove?.Dispose();
        disposableShot?.Dispose();
    }

    
    private void UpdateRotate()
    {
        // 本体の回転
        trRoot.localRotation = Quaternion.Euler(0, 0, (trRoot.localRotation.eulerAngles.z) - 180.0f * Time.deltaTime);
    }

    private void UpdateMove()
    {
        // 8の字に動かす
        Vector3 pos = transform.localPosition;
        moves += Mathf.PI * Time.deltaTime;
        pos.x = BasePos.x + 200 * Mathf.Cos(moves * 0.25f);
        pos.y = BasePos.y + 100 * Mathf.Sin(moves * 0.25f * 2);
        transform.localPosition = pos;
    }

    private void UpdateShot()
    {
        if (counter == 0)
        {
            ShotWay(StgBulletController.BulletType.CirclePink_LL, 1, 0, transform.localPosition, AppUtil.PostToDeg(GetLeftPlayerTransform().localPosition - transform.localPosition), 5);

        }
        else
        {
            ShotWay(StgBulletController.BulletType.CircleBlue_LL, 1, 0, transform.localPosition, AppUtil.PostToDeg(GetRightPlayerTransform().localPosition - transform.localPosition), 5);
        }

        counter++;
    }
}
