﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UniRx.Async;

public class StgHexAnimationController : MonoBehaviour
{
    [SerializeField] private Transform trRoot = default;
    [SerializeField] private SpriteRenderer spriteRenderer = default;

    
    public async void Play(Vector3 scale)
    {
        trRoot.localScale = Vector3.zero;
        trRoot.localRotation = Quaternion.Euler(0, 0, Random.Range(0, 359));
        spriteRenderer.color = new Color32(236, 54, 32, 255);

        trRoot.DOScale(scale, 0.25f).SetEase(Ease.OutExpo);
        spriteRenderer.DOFade(0.0f, 0.25f);

        await UniTask.Delay(250);

        StgHexAnimationFactory.Instance.Return(this);
    }
}
