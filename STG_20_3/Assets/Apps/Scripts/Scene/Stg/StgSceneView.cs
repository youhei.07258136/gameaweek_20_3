﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;
using Coffee.UIExtensions;

public class StgSceneView : MonoBehaviour
{
    [SerializeField] private StgStartAnimation startAnim = default;
    [SerializeField] private StgEndAnimation endAnim = default;

	[SerializeField] private StgEnemyLifeGauge enemyLifeGauge = default;
    [SerializeField] private StgLogger logger = default;
	[SerializeField] private Text txtScore = default;
	[SerializeField] private Text txtPlayerLife = default;
    [SerializeField] private Image imgFade = default;
    [SerializeField] private UIEffect renderEffect = default;
    [SerializeField] private Button btnBackToTitle = default;

    private Tweener doEffectFactor = null;

	public void PlayStartAnimation(int stageNum, Action endCallback)
	{
		startAnim.Play(stageNum, endCallback);

        float end = (stageNum+1) % 2;

        if (doEffectFactor != null)
            doEffectFactor.Kill(true);

        doEffectFactor = DOTween.To(() => renderEffect.effectFactor, val => renderEffect.effectFactor = val, end, 1.0f).SetEase(Ease.InOutCirc);
    }

    public void PlayEndAnimation(Action endCallback)
    {
        endAnim.Play(endCallback);
    }

	public void StartEnemyLifeGauge(Func<float> func)
	{
		enemyLifeGauge.Setup(func);
	}

	public void SetSccore(long score)
	{
		txtScore.text = string.Format("SCORE:{0:D12}", score);
	}

	public void SetPlayerLife(int life)
	{
		txtPlayerLife.text = "PLAYER LIFE x " + life.ToString();
	}

    public void AddSystemLog(string log)
    {
        logger.AddSystemLog(log);
    }

    public void SetFade(float alpha)
    {
        imgFade.color = new Color(0, 0, 0, alpha);
    }

    public void StartFade(float end, Action callback = null)
    {
        imgFade.DOKill(true);

        imgFade.DOColor(new Color(0, 0, 0, end), 0.25f).OnComplete(()=> { callback?.Invoke(); }).SetAutoKill(true);
    }

    public void ActiveBackToTitleButton(Action callback)
    {
        btnBackToTitle.gameObject.SetActive(true);
        btnBackToTitle.onClick.AddListener(() => { callback?.Invoke(); });
    }
}
