﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System;
using UnityEngine;
using UnityEngine.U2D;
using UniRx.Async;
using DG.Tweening;
using Photon.Pun;
using Photon.Realtime;

public class StgEnemyControllerBase : MonoBehaviourPunCallbacks
{
    [SerializeField] protected Collider2D mainCollider = default;

    protected int Life { get => lifeFunc(); }
    protected int StageNum { get => stageNumFunc(); }

	private Action deadCallback = null;
	private Action hitCallback = null;

    private Func<int> lifeFunc;
    private Func<int> stageNumFunc;
    private Coroutine process = null;

    protected List<Transform> playerTransforms;

    public override void OnDisable()
    {
        base.OnDisable();
        if (process != null) StopCoroutine(process);
        process = null;
        transform.DOKill(true);
        gameObject.SetActive(false);
        OnDead();
    }

    public void Setup(int viewID, List<Transform> playerTransforms, Func<int> lifeFunc, Func<int> stageNumFunc, Action hitCallback, Action deadCallback)
    {
		photonView.ViewID = viewID;

        this.lifeFunc = lifeFunc;
        this.stageNumFunc = stageNumFunc;
        this.hitCallback = hitCallback;
		this.deadCallback = deadCallback;
        this.playerTransforms = playerTransforms;

		if(process != null) StopCoroutine(process);
        process = StartCoroutine(Process());
    }

    protected void ShotWay(StgBulletController.BulletType type, int way, float space, Vector2 beginPos, float rotateDeg, float time, Ease ease = Ease.Linear)
    {
        float startRot = -((float)(way-1) * space) / 2.0f  + rotateDeg;

        StgBulletFactory bulletFactory = StgBulletFactory.Instance;

        for(int i = 0; i < way; ++i)
        {
            bulletFactory
                .Rent(StgBulletController.FromType.Enemy)
                .MoveEase(type, beginPos, startRot + (space * i), time, ease);
        }
    }

    protected Transform GetLeftPlayerTransform()
    {
        float val = 9999;
        Transform ret = null;
        for(int i = 0; i < playerTransforms.Count; ++i)
        {
            Vector3 p = playerTransforms[i].localPosition - transform.localPosition;

            if(p.x <= val)
            {
                val = p.x;
                ret = playerTransforms[i];
            }
        }

        return ret;
    }
    protected Transform GetRightPlayerTransform()
    {
        float val = -9999;
        Transform ret = null;
        for (int i = 0; i < playerTransforms.Count; ++i)
        {
            Vector3 p = playerTransforms[i].localPosition - transform.localPosition;

            if (p.x >= val)
            {
                val = p.x;
                ret = playerTransforms[i];
            }
        }

        return ret;
    }


    protected void OnBulletHit()
    {
        hitCallback?.Invoke();

        if (Life <= 0 && gameObject.activeSelf)
		{
            if (process != null) StopCoroutine(process);
            process = null;
			transform.DOKill(true);
            gameObject.SetActive(false);
            StgHexAnimationFactory.Instance.Rent(transform.position).Play(Vector3.one * 8);
            OnDead();
			deadCallback?.Invoke();
		}
    }

	protected virtual void OnDead()
	{
	}

    protected virtual IEnumerator Process()
    {
		yield break;
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == AppDefine.StgEnemyBulletTag)
            return;

        if (collision.gameObject.tag != AppDefine.StgPlayerMineBulletTag)
            return;

        OnBulletHit();

        StgBulletController stgBulletController = collision.gameObject.GetComponent<StgBulletController>();
        if (stgBulletController == null)
            return;

        stgBulletController.PlayHexAnimation();
        StgBulletFactory.Instance.Return(stgBulletController);
    }

    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        base.OnMasterClientSwitched(newMasterClient);
        photonView.TransferOwnership(newMasterClient);
    }

    public void OnOwnershipRequest(object[] viewAndPlayer)
    {
        PhotonView view = viewAndPlayer[0] as PhotonView;
        Player requestingPlayer = viewAndPlayer[1] as Player;
    }
}
