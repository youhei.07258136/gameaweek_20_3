﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UniRx.Async;

public class StgEndAnimation : UGUIBehaviour
{
    [SerializeField] private RectTransform rtFrame = default;
    [SerializeField] private Image imgBg = default;
    [SerializeField] private Text txtGameOver = default;

    Color textDefaultColor = default;

    private void Awake()
    {
        imgBg.color = Color.clear;
        textDefaultColor = txtGameOver.color;
    }

    public async void Play(Action endCallback)
    {
        // Init
        gameObject.SetActive(true);
        rtFrame.localScale = new Vector3(1, 0);
        txtGameOver.color = Color.clear;

        // Start
        imgBg.DOFade(0.5f, 0.5f);
        txtGameOver.DOColor(textDefaultColor, 0.5f);
        rtFrame.DOScale(Vector3.one, 0.5f).SetEase(Ease.InOutCirc);
        await UniTask.Delay(500);

        // End

        // Final
        endCallback?.Invoke();
    }
}
