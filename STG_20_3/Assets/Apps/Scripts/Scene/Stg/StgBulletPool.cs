﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Toolkit;

public class StgBulletPool : ObjectPool<StgBulletController>
{
    private GameObject prefab = null;
    private Transform parent = null;

    public StgBulletPool(GameObject prefab, Transform parent)
    {
        this.prefab = prefab;
        this.parent = parent;
    }

    protected override StgBulletController CreateInstance()
    {
        StgBulletController bullet = Object.Instantiate(prefab).GetComponent<StgBulletController>();
        bullet.transform.parent = parent;

        return bullet;
    }

    protected override void OnBeforeReturn(StgBulletController instance)
    {
        base.OnBeforeReturn(instance);
        instance.transform.parent = parent;
    }
}