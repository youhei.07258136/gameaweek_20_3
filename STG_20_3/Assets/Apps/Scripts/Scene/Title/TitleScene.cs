﻿using System.Collections;
using System.Collections.Generic;
using UniRx.Async;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class TitleScene : SceneBehaviour
{
    [Serializable]
    public class RankTextData
    {
        public Text txtNum;
        public Text txtDate;
        public Text txtScore;
    }

    [SerializeField] private Button btnScreeen = default;
    [SerializeField] private Text txtTouchToScreen = default;
    [SerializeField] private RankTextData[] txtRankDatas = default;

    internal override async UniTask OnChangeSceneAwake(object obj)
    {
        await base.OnChangeSceneAwake(obj);

        txtTouchToScreen.color = new Color(1, 1, 1, 0);

        btnScreeen.interactable = false;

        for(int i = 0; i < txtRankDatas.Length; ++i)
        {
            RankTextData d = txtRankDatas[i];

            d.txtNum.color = d.txtDate.color = d.txtScore.color = new Color(1, 1, 1, 0);
            d.txtDate.text = AppSaveData.Rankings[i].dateText;
            d.txtScore.text = string.Format("{0:D12}", AppSaveData.Rankings[i].score);
        }
    }

    internal override async UniTask OnChangeSceneStart()
    {
        await base.OnChangeSceneStart();

        await UniTask.DelayFrame(3);

        SceneTransitionManager.Instance.SetVisibleLoading(false);

        for (int i = 0; i < txtRankDatas.Length; ++i)
        {
            RankTextData d = txtRankDatas[i];

            d.txtNum.DOFade(1.0f, 0.8f);
            d.txtDate.DOFade(1.0f, 0.8f);
            d.txtScore.DOFade(1.0f, 0.8f);

            await UniTask.Delay(200);
        }
        btnScreeen.interactable = true;

        btnScreeen.onClick.AddListener(() => { SceneTransitionManager.Instance.ChangeScene(1); });
        txtTouchToScreen.DOFade(1.0f, 1.0f).SetLoops(-1, LoopType.Yoyo);
    }
}
