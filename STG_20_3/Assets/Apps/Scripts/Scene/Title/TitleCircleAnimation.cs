﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleCircleAnimation : MonoBehaviour
{
    [SerializeField] Transform circle1 = default;
    [SerializeField] Transform circle2 = default;

    private void Update()
    {
        circle1.localRotation = Quaternion.Euler(0, 0, circle1.localRotation.eulerAngles.z + 2.0f * Time.deltaTime);
        circle2.localRotation = Quaternion.Euler(0, 0, circle2.localRotation.eulerAngles.z - 4.0f * Time.deltaTime);
    }
}
