﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx.Async;
using System.IO;

/// <summary>
/// サウンドの管理クラス
/// </summary>
public class SoundManager : SingletonMonoBehaviour<SoundManager>
{
    [SerializeField] private CriWareInitializer criWareInitializer = default;   //!< CRIの初期化機
    [SerializeField] private string fileDirectory = default;                    //!< *.acb, *.awbの格納先 
    [SerializeField] private string bgmCueSheetName = default;                  //!< 基本のBGMシート名
    [SerializeField] private string seCueSheetName = default;                   //!< 基本のSEシート名
    [SerializeField] private int bgmPlayerNum = 2;                              //!< BGMの同時再生数

    public CriAtomConfig AtomConfing { get; private set;  }             //!< 設定値取得

    private Dictionary<string, CriAtomCueSheet> dictCueSheet = null;    //!< 管理中のキューシート
    private CriAtomExPlayer[] bgmPlayers = null;                        //!< BGMの再生プレイヤー
    private CriAtomExPlayer sePlayer = null;                            //!< SEの再生プレイヤー
    private string[] prevPlayBgmCueNames = null;                        //!< 直前に再生したBGM名

    /// <summary>
    /// Awake
    /// </summary>
    protected override void Awake()
    {
        base.Awake();

        // CRI初期化
        criWareInitializer.Initialize();

        // プレイヤー生成
        bgmPlayers = new CriAtomExPlayer[bgmPlayerNum];
        prevPlayBgmCueNames = new string[bgmPlayerNum];
        for (int i = 0; i < bgmPlayerNum; ++i)
        {
            bgmPlayers[i] = new CriAtomExPlayer();

			// BGMはフェード対応
			bgmPlayers[i].AttachFader();
			bgmPlayers[i].SetFadeInTime(1000);
			bgmPlayers[i].SetFadeOutTime(1000);
		}
		sePlayer = new CriAtomExPlayer();

        // メインのサウンド読み込み
        dictCueSheet = new Dictionary<string, CriAtomCueSheet>();
        if (bgmCueSheetName.Length > 0) AddCueSheet(bgmCueSheetName);
        if (seCueSheetName.Length > 0)  AddCueSheet(seCueSheetName);

        AtomConfing = criWareInitializer.atomConfig;
    }

	/// <summary>
	/// Sets the bgm volume.
	/// </summary>
	/// <param name="volume">Volume.</param>
	public void SetBgmVolume(float volume)
	{
		if(bgmPlayers == null)
			return;

		foreach(CriAtomExPlayer player in bgmPlayers)
		{
			player.SetVolume(volume);
            player.UpdateAll();
		}
	}

	/// <summary>
	/// Ses the se volume.
	/// </summary>
	/// <param name="volume">Volume.</param>
	public void SetSeVolume(float volume)
	{
		if(sePlayer == null)
			return;

		sePlayer.SetVolume(volume);
        sePlayer.UpdateAll();
	}

    /// <summary>
    /// キューシート追加
    /// </summary>
    /// <param name="cueSheetName">キューシート名</param>
    public void AddCueSheet(string cueSheetName)
    {
        if (dictCueSheet.ContainsKey(cueSheetName))
            return;

        string acbFileName = fileDirectory + "/" + cueSheetName + ".acb";
        string awbFileName = fileDirectory + "/" + cueSheetName + ".awb";

        CriAtomCueSheet cueSheet = CriAtom.AddCueSheet(cueSheetName, acbFileName, awbFileName);

        dictCueSheet.Add(cueSheetName, cueSheet);
    }

    /// <summary>
    /// キューシートの追加 Async版
    /// </summary>
    /// <param name="cueSheetName">キューシート名</param>
    /// <returns></returns>
    public async UniTask AddCueSheetAsync(string cueSheetName)
    {
        string acbFileName = fileDirectory + "/" + cueSheetName + ".acb";
        string awbFileName = fileDirectory + "/" + cueSheetName + ".awb";

        CriAtomCueSheet cueSheet = CriAtom.AddCueSheetAsync(cueSheetName, acbFileName, awbFileName);
        await UniTask.WaitUntil(() => { return !cueSheet.IsLoading; });

        dictCueSheet.Add(cueSheetName, cueSheet);
    }

    /// <summary>
    /// キューシートを取り除く
    /// </summary>
    /// <param name="cueSheetName">キューシート名</param>
    public void RemoveCueSheet(string cueSheetName)
    {
        CriAtom.RemoveCueSheet(cueSheetName);

        dictCueSheet.Remove(cueSheetName);
    }

    public CriAtomExAcb GetAcb(string cueSheetName)
    {
        if (!dictCueSheet.ContainsKey(cueSheetName))
            return null;

        return dictCueSheet[bgmCueSheetName].acb;
    }

    /// <summary>
    /// 簡易BGM再生
    /// </summary>
    /// <param name="cueName">キュー名</param>
    /// <param name="index">インデックス</param>
    public void PlayBgm(string cueName, int index = 0)
    {
        if (prevPlayBgmCueNames[index] == cueName)
            return;

        bgmPlayers[index].Stop();

        CriAtomExAcb acb = dictCueSheet[bgmCueSheetName].acb;
        bgmPlayers[index].SetCue(acb, cueName);
        bgmPlayers[index].Loop(true);
        bgmPlayers[index].Start();

        prevPlayBgmCueNames[index] = cueName;
    }

    /// <summary>
    /// 簡易BGM停止
    /// </summary>
    /// <param name="index">インデックス</param>
    public void StopBgm(int index = 0)
    {
        bgmPlayers[index].Stop();

        prevPlayBgmCueNames[index] = "";
    }

    /// <summary>
    /// 簡易SE再生
    /// </summary>
    /// <param name="cueName">キュー名</param>
    public void PlaySeOneShot(string cueName)
    {
        CriAtomExAcb acb = dictCueSheet[seCueSheetName].acb;
        sePlayer.SetCue(acb, cueName);
        sePlayer.Loop(false);
        sePlayer.Start();
    }
}
