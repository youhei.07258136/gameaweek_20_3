﻿using UnityEngine;
using System;

/// <summary>
/// ダイアログの管理クラス
/// </summary>
public class DialogManager : SingletonMonoBehaviour<DialogManager>
{
    /// <summary>
    /// ダイアログの表示ターゲット
    /// </summary>
    public enum DialogTarget
    {
        System, //!< システム
        Modal,  //!< モーダル
    }

    /// <summary>
    /// 取り付ける先のtransform
    /// </summary>
    [SerializeField, EnumLabel(typeof(DialogTarget))]
    private Transform[] targetTransform = new Transform[Enum.GetValues(typeof(DialogTarget)).Length];

    [SerializeField] private DialogSimpleText prefabSimpleText = default;   //!< 簡易表示用のモーダルprefab

    /// <summary>
    /// 開いているダイアログがあるか確認
    /// </summary>
    public bool HasOpenDaialog
    {
        get
        {
            foreach(Transform tr in targetTransform)
            {
                if (tr.childCount > 0)
                    return true;
            }

            return false;
        }
    }

    /// <summary>
    /// ダイアログを開く
    /// </summary>
    /// <typeparam name="T">DialogBaseの継承型</typeparam>
    /// <param name="obj">Instantiateする対象</param>
    /// <param name="target">配属先</param>
    /// <returns>開いたダイアログ</returns>
    public T OpenDialog<T>(T obj, DialogTarget target) where T : DialogBase
    {
        T ret = Instantiate<T>(obj, targetTransform[(int)target]);
        ret.OpenDialog();

        return ret;
    }

    /// <summary>
    /// テキストダイアログを開く
    /// </summary>
    /// <param name="title">タイトル</param>
    /// <param name="description">内容</param>
    /// <param name="useScroll">スクロールするか</param>
    /// <param name="target">配属先</param>
    /// <returns>開いたダイアログ</returns>
    public DialogSimpleText OpenTextDialog(string title, string description, bool useScroll = false, DialogTarget target = DialogTarget.Modal)
    {
        DialogSimpleText ret = OpenDialog(prefabSimpleText, target);

        ret.Title = title;
        ret.Description = description;
        ret.SetScrollAcrtive(useScroll);

        return ret;
    }

    /// <summary>
    /// 指定した先のダイアログをすべて閉じる
    /// </summary>
    /// <param name="target">配属先</param>
    public void CloseTargetAll(DialogTarget target = DialogTarget.Modal)
    {
        for(int i = 0; i < targetTransform[(int)target].childCount; ++i)
        {
            Destroy(targetTransform[(int)target].GetChild(i).gameObject);
        }
    }
}
