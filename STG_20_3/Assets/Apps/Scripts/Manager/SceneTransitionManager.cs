﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UniRx;
using UniRx.Async;
using DG.Tweening;

/// <summary>
/// シーン遷移時に色々呼び出されるBehaviour
/// </summary>
public class SceneBehaviour : MonoBehaviour
{
    // SceneTransitionManager.ChangeSceneコール時に呼び出し元のシーンでコール
    internal virtual async UniTask OnChangeSceneEnd() { await UniTask.Run(() => { }); }       //!< ChangeSceneコール直後くらいに呼ばれる
    internal virtual async UniTask OnChangeSceneDestroy() { await UniTask.Run(() => { }); }   //!< 次のシーンに移行する直前くらいに呼ばれる

    // SceneTransitionManager.ChangeSceneコール時に呼び出し先のシーンでコール
    internal virtual async UniTask OnChangeSceneAwake(object obj) { await UniTask.Run(() => { }); }    //!< シーンが読み込み終わった直後に呼ばれる
    internal virtual async UniTask OnChangeSceneStart() { await UniTask.Run(() => { }); }              //!< フェードが明けた後に呼ばれる

    protected virtual void Awake()
    {
        if (SceneTransitionManager.Instance == null)
			return;

        SceneTransitionManager.Instance.AddSceneBehaviour(this);
	}
}

/// <summary>
/// シーン遷移と移行アニメーション管理
/// </summary>
public class SceneTransitionManager : SingletonMonoBehaviour<SceneTransitionManager>
{
	public const float FadeTime = 0.4f;

    [SerializeField] FadeImage screenFade = default;	//!< フェード
	[SerializeField] Text txtNowLoading = default;		//!< NowLoading
	[SerializeField] Text txtNowLoadingDot = default;	//!< ...

	/// <summary>
	/// Gets a value indicating whether this <see cref="T:SceneTransitionManager"/> is changeng.
	/// </summary>
	/// <value><c>true</c> if is changeng; otherwise, <c>false</c>.</value>
	public bool IsChangeng { get; private set; }

    private List<SceneBehaviour> sceneBehaviourList = new List<SceneBehaviour>(); //!< 通知対象リスト

	protected override void Awake()
	{
		base.Awake();
		DontDestroyOnLoad(this);

		IsChangeng = false;

        if (txtNowLoading != null)
        {
            txtNowLoading.enabled =
            txtNowLoadingDot.enabled = false;
        }
	}

    /// <summary>
    /// 初回シーン初期化
    /// </summary>
    public async void InitializeScene()
    {
        // 次のシーンのロード完了イベントをコール
        if (sceneBehaviourList.Count > 0)
        {
            foreach (SceneBehaviour sb in sceneBehaviourList)
            {
                await sb.OnChangeSceneAwake(null);
            }
        }

        // 次のシーンの開始イベントをコール
        if (sceneBehaviourList.Count > 0)
        {
            foreach (SceneBehaviour sb in sceneBehaviourList)
            {
                await sb.OnChangeSceneStart();
            }
        }
    }

    /// <summary>
    /// 現在ロード中のところからNowLoadingを途中で消したい時に使う
    /// </summary>
    /// <param name="isVisible"></param>
    public void SetVisibleLoading(bool isVisible)
    {
        txtNowLoading.enabled =
        txtNowLoadingDot.enabled = isVisible;
    }

    /// <summary>
	/// Adds the scene behaviour.
	/// </summary>
	/// <param name="sb">Sb.</param>
    public void AddSceneBehaviour(SceneBehaviour sb)
    {
        if (sceneBehaviourList.Find(obj => { return obj == sb; }))
            return;

        sceneBehaviourList.Add(sb);
    }

    /// <summary>
    /// Removes the scene behaviour.
    /// </summary>
    /// <param name="sb">Sb.</param>
    public void RemoveSceneBehaviour(SceneBehaviour sb)
    {
        if (!sceneBehaviourList.Find(obj => { return obj != sb; }))
            return;

        sceneBehaviourList.Remove(sb);
    }

    /// <summary>
    /// Changes the scene.
    /// </summary>
    /// <param name="sceneIndex">Scene index.</param>
    /// <param name="obj">Object.</param>
    public void ChangeScene(int sceneIndex, object obj = null)
    {
        if (IsChangeng) return;
        ChangeSenceAsync(sceneIndex, obj);
    }

	/// <summary>
	/// Changes the sence async.
	/// </summary>
	/// <param name="nextSceneIndex">Next scene index.</param>
	/// <param name="obj">Object.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
    private async void ChangeSenceAsync<T>(int nextSceneIndex, T obj)
    {
        IsChangeng = true;

        // NowLoading表記を開始する
        if (txtNowLoading != null)
        {
            txtNowLoading.enabled =
            txtNowLoadingDot.enabled = true;

            txtNowLoadingDot.text = "";
            txtNowLoadingDot
                .DOText("...　 ", 1.5f, false, ScrambleMode.None)
                .OnStart(() => { txtNowLoadingDot.text = ""; })
                .SetLoops(-1);
        }

		// 前のシーンのフェードアウト開始イベントをコール
		if (sceneBehaviourList.Count > 0)
        {
            foreach (SceneBehaviour sb in sceneBehaviourList)
            {
                await sb.OnChangeSceneEnd();
            }
        }

        // フェードアウト
		DOTween.To(() => 0.0f, v => screenFade.Range = v, 1.0f, FadeTime);
		await UniTask.Delay((int)(FadeTime * 1000.0f));

		// 前のシーンの終了イベントをコール
		if (sceneBehaviourList.Count > 0)
        {
            foreach (SceneBehaviour sb in sceneBehaviourList)
            {
                await sb.OnChangeSceneDestroy();
            }
        }
       
        sceneBehaviourList.Clear();

        // シーン読み込み開始
        await SceneManager.LoadSceneAsync(nextSceneIndex);
        screenFade.Range = 1.0f;

        // 次のシーンのロード完了イベントをコール
        if (sceneBehaviourList.Count > 0)
        {
            foreach (SceneBehaviour sb in sceneBehaviourList)
            {
                await sb.OnChangeSceneAwake(obj);
            }
        }

        // フェードイン
        DOTween.To(() => 1.0f, v => screenFade.Range = v, 0.0f, FadeTime);
		await UniTask.Delay((int)(FadeTime * 1000.0f));

        // 次のシーンの開始イベントをコール
        if (sceneBehaviourList.Count > 0)
        {
            foreach (SceneBehaviour sb in sceneBehaviourList)
            {
                await sb.OnChangeSceneStart();
            }
        }

        // Now Loading表記を終了する
        if (txtNowLoading != null)
        {
            txtNowLoading.enabled =
            txtNowLoadingDot.enabled = false;
            txtNowLoadingDot.DOKill(true);
        }

		IsChangeng = false;
	}
}
