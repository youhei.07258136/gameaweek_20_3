﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AppDefine
{
    public const int StgStageWidth = 720;
    public const int StgStageHeight = 1280;
    public const long StgScoreMax = 999999999999;

    public static readonly string StgEnemyBulletTag = "EnemyBullet";
    public static readonly string StgPlayerMineBulletTag = "PlayerBullet_Mine";
    public static readonly string StgPlayerOtherBulletTag = "PlayerBullet_Mine";
}
